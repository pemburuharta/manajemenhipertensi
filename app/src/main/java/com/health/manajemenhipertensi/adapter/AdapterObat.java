package com.health.manajemenhipertensi.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.Helper;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.activity.AddAktivitasActivity;
import com.health.manajemenhipertensi.activity.AddObatActivity;
import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.model.ModelObat;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AdapterObat extends RecyclerView.Adapter<AdapterObat.ViewHolder> {

    private Context context;
    private ArrayList<ModelObat> list;

    SharedPref pref;

    public AdapterObat(Context context, ArrayList<ModelObat> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_obat,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        ModelObat m = list.get(position);
        pref = new SharedPref(context);

        holder.listobatTvAktifitas.setText(m.getTMOKETERSEDIAAN() + " "+ m.getTMONAMA());
        holder.listobatTvDosis.setText(m.getTMODOSIS() + " - " + m.getTMOFREKUENSI());
        holder.listobatTvJam.setText(m.getTMOWAKTU());
        holder.listobatTvTanggal.setText(""+new Helper().convertDateFormat2(m.getTMOSTARTTGL(), "yyyy-MM-dd") +" - "+ ""+new Helper().convertDateFormat2(m.getTMOENDTGL(), "yyyy-MM-dd"));

        holder.listobatCvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (pref.getRule().equals("USER")){
                    final CharSequence[] dialogItem = {"Edit", "Hapus"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Tentukan Pilihan Anda");
                    builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, final int wich) {
                            switch (wich) {
                                case 0:
                                    Intent intent = new Intent(context, AddObatActivity.class);
                                    intent.putExtra("status", "EDIT");
                                    intent.putExtra("data", list.get(position));
                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                    break;
                                case 1:
                                    new AlertDialog.Builder(view.getContext())
                                            .setMessage("Apakah anda akan menghapus pengingat ini ?")
                                            .setCancelable(false)
                                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    HapusObat(position);
                                                }
                                            })
                                            .setNegativeButton("Tidak", null)
                                            .show();
                                    break;
                            }
                        }
                    });
                    builder.create().show();
                }
            }
        });

    }

    private void HapusObat(int position) {

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.deleteObat(list.get(position).getTMOBIGID()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    if (error.equals("false")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        Toast.makeText(context, "Aktifitas Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Aktifitas Gagal Dihapus", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView listobatCvMain;
        private TextView listobatTvAktifitas;
        private TextView listobatTvDosis;
        private TextView listobatTvJam;
        private TextView listobatTvTanggal;
        public ViewHolder(View itemView) {
            super(itemView);
            listobatCvMain = itemView.findViewById(R.id.listobat_cv_main);
            listobatTvAktifitas = itemView.findViewById(R.id.listobat_tv_aktifitas);
            listobatTvDosis = itemView.findViewById(R.id.listobat_tv_dosis);
            listobatTvJam = itemView.findViewById(R.id.listobat_tv_jam);
            listobatTvTanggal = itemView.findViewById(R.id.listobat_tv_tanggal);
        }
    }

}