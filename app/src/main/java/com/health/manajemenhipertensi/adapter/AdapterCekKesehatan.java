package com.health.manajemenhipertensi.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.Helper;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.activity.AddAktivitasActivity;
import com.health.manajemenhipertensi.activity.AddCekKesehatanActivity;
import com.health.manajemenhipertensi.model.ModelCekKesehatan;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AdapterCekKesehatan extends RecyclerView.Adapter<AdapterCekKesehatan.ViewHolder> {

    private Context context;
    private ArrayList<ModelCekKesehatan> list;
    SharedPref pref;

    String ID;

    public AdapterCekKesehatan(Context context, ArrayList<ModelCekKesehatan> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cek_kesehatan,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        ModelCekKesehatan m = list.get(position);
        pref = new SharedPref(context);

        ID = m.getTKBIGID();

        holder.listcekkesehatanTvTempat.setText(m.getTKTEMPAT());
        holder.listcekkesehatanTvJam.setText(m.getTKWAKTU());
        holder.listcekkesehatanTvTanggal.setText(""+new Helper().convertDateFormat(m.getTKTGL(), "yyyy-MM-dd"));

        holder.listcekkesehatanCvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (pref.getRule().equals("USER")){
                    final CharSequence[] dialogItem = {"Edit", "Hapus"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Tentukan Pilihan Anda");
                    builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, final int wich) {
                            switch (wich) {
                                case 0:
                                    Intent intent = new Intent(context, AddCekKesehatanActivity.class);
                                    intent.putExtra("status", "EDIT");
                                    intent.putExtra("data", list.get(position));
                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                    break;
                                case 1:
                                    new AlertDialog.Builder(view.getContext())
                                            .setMessage("Apakah anda akan menghapus pengingat ini ?")
                                            .setCancelable(false)
                                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    HapusKesehatan(position);
                                                }
                                            })
                                            .setNegativeButton("Tidak", null)
                                            .show();
                                    break;

                            }
                        }
                    });
                    builder.create().show();
                }
            }
        });

    }

    private void HapusKesehatan(int id) {

//        final ProgressDialog pd;
//        pd = new ProgressDialog(context);
//        pd.setMessage("Loading...");
//        pd.setCancelable(false);
//        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.deleteKesehatan(
                "delete",
                list.get(id).getTKBIGID())
                .enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    if (error.equals("false")) {
//                        pd.dismiss();
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        Toast.makeText(context, "Pengingat Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                    } else {
//                        pd.dismiss();
                        Toast.makeText(context, "Pengingat Gagal Dihapus", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                pd.dismiss();
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView listcekkesehatanCvMain;
        private TextView listcekkesehatanTvJam;
        private TextView listcekkesehatanTvTanggal;
        private TextView listcekkesehatanTvTempat;
        public ViewHolder(View itemView) {
            super(itemView);
            listcekkesehatanCvMain = itemView.findViewById(R.id.listcekkesehatan_cv_main);
            listcekkesehatanTvJam = itemView.findViewById(R.id.listcekkesehatan_tv_jam);
            listcekkesehatanTvTanggal = itemView.findViewById(R.id.listcekkesehatan_tv_tanggal);
            listcekkesehatanTvTempat = itemView.findViewById(R.id.listcekkesehatan_tv_tempat);
        }
    }

}