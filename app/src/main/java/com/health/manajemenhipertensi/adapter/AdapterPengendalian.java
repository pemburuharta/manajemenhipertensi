package com.health.manajemenhipertensi.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.Helper;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.activity.AddPengingatDietActivity;
import com.health.manajemenhipertensi.activity.MainAdminActivity;
import com.health.manajemenhipertensi.model.ModelPengendalian;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AdapterPengendalian extends RecyclerView.Adapter<AdapterPengendalian.ViewHolder> {

    private Context context;
    private ArrayList<ModelPengendalian> list;

    SharedPref pref;

    public AdapterPengendalian(Context context, ArrayList<ModelPengendalian> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pengendalian,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        pref = new SharedPref(context);

        holder.cardPengendalian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (pref.getRule().equals("USER")){
                    final CharSequence[] dialogItem = {"Edit", "Hapus"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Tentukan Pilihan Anda");
                    builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, final int wich) {
                            switch (wich) {
                                case 0:
                                    Intent intent = new Intent(context, AddPengingatDietActivity.class);
                                    intent.putExtra("status", "EDIT");
                                    intent.putExtra("data", list.get(position));
                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                    break;
                                case 1:
                                    new AlertDialog.Builder(view.getContext())
                                            .setMessage("Apakah anda akan menghapus pengingat ini ?")
                                            .setCancelable(false)
                                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    HapusPengendalian(position);
                                                }
                                            })
                                            .setNegativeButton("Tidak", null)
                                            .show();
                                    break;
                            }
                        }
                    });
                    builder.create().show();
                }
            }
        });

        holder.pengendalianTvCreatedat.setText(""+new Helper().convertDateFormat(list.get(position).getTRCREATEDAT(),"yyyy-MM-dd hh:mm:s"));
        holder.judulPengendalian.setText(list.get(position).getTRNAMA());
        Picasso.with(context)
                .load("http://reminder.mitraredex.com/assets/kendali/"+list.get(position).getTRIMAGE())
                .into(holder.imgPengendalian);

    }

    private void HapusPengendalian(int position) {

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.deleteKendali(list.get(position).getTRBIGID()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    if (error.equals("false")) {
//                        pd.dismiss();
                        Intent intent = new Intent(context, MainAdminActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        Toast.makeText(context, "Pengendalian Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                    } else {
//                        pd.dismiss();
                        Toast.makeText(context, "Pengendalian Gagal Dihapus", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout cardPengendalian;
        private ImageView imgPengendalian;
        private TextView judulPengendalian;
        private TextView pengendalianTvCreatedat;
        public ViewHolder(View itemView) {
            super(itemView);
            cardPengendalian = itemView.findViewById(R.id.card_pengendalian);
            imgPengendalian = itemView.findViewById(R.id.img_pengendalian);
            judulPengendalian = itemView.findViewById(R.id.judul_pengendalian);
            pengendalianTvCreatedat = itemView.findViewById(R.id.pengendalian_tv_createdat);
        }
    }

}