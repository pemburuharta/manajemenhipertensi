package com.health.manajemenhipertensi.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.Helper;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.activity.AddKategoriActivity;
import com.health.manajemenhipertensi.activity.AddPengingatDietActivity;
import com.health.manajemenhipertensi.activity.DetailDefinisiActivity;
import com.health.manajemenhipertensi.activity.KategoriActivity;
import com.health.manajemenhipertensi.activity.MainAdminActivity;
import com.health.manajemenhipertensi.activity.MateriActivity;
import com.health.manajemenhipertensi.model.ModelKategori;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AdapterKategori extends RecyclerView.Adapter<AdapterKategori.ViewHolder> {

    private Context context;
    private ArrayList<ModelKategori> list;

    SharedPref pref;

    public AdapterKategori(Context context, ArrayList<ModelKategori> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kategori,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        pref = new SharedPref(context);

            holder.cardKategori.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    if (pref.getRule().equals("USER")){
                        Intent intent = new Intent(context, MateriActivity.class);
                        intent.putExtra("id",""+list.get(position).getTKBIGID());
                        context.startActivity(intent);
                    } else {
                        final CharSequence[] dialogItem = {"Edit", "Hapus"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Tentukan Pilihan Anda");
                        builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, final int wich) {
                                switch (wich) {
                                    case 0:
                                        Intent intent = new Intent(context, AddKategoriActivity.class);
                                        intent.putExtra("status", "EDIT");
                                        intent.putExtra("data", list.get(position));
                                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(intent);
                                        break;
                                    case 1:
                                        new AlertDialog.Builder(view.getContext())
                                                .setMessage("Apakah anda akan menghapus  ini ?")
                                                .setCancelable(false)
                                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        HapusKategori(position);
                                                    }
                                                })
                                                .setNegativeButton("Tidak", null)
                                                .show();
                                        break;
                                }
                            }
                        });
                        builder.create().show();
                    }
                }
            });

        holder.kategoriTvCreatedat.setText(""+new Helper().convertDateFormat(list.get(position).getTKCREATEDAT(),"yyyy-MM-dd hh:mm:s"));
        holder.kategoriJudul.setText(list.get(position).getTKNAMA());
        Picasso.with(context)
                .load("http://reminder.mitraredex.com/assets/kategori/"+list.get(position).getTKIMAGE())
                .into(holder.imgKategori);

    }

    private void HapusKategori(int position) {

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.deleteKategori(list.get(position).getTKBIGID()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    if (error.equals("false")) {
//                        pd.dismiss();
                        Intent intent = new Intent(context, MainAdminActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        Toast.makeText(context, "Kategori Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                    } else {
//                        pd.dismiss();
                        Toast.makeText(context, "Kategori Gagal Dihapus", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout cardKategori;
        private ImageView imgKategori;
        private TextView kategoriJudul;
        private TextView kategoriTvCreatedat;
        public ViewHolder(View itemView) {
            super(itemView);
            cardKategori = itemView.findViewById(R.id.card_kategori);
            imgKategori = itemView.findViewById(R.id.img_kategori);
            kategoriJudul = itemView.findViewById(R.id.kategori_judul);
            kategoriTvCreatedat = itemView.findViewById(R.id.kategori_tv_createdat);
        }
    }

}