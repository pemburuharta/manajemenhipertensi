package com.health.manajemenhipertensi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.health.manajemenhipertensi.Helper.Helper;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.model.ModelUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder> {

    private Context context;
    private ArrayList<ModelUser> list;

    SharedPref pref;

    public AdapterUser(Context context, ArrayList<ModelUser> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        pref = new SharedPref(context);
        final ModelUser m = list.get(position);

        Picasso.with(context)
                .load("http://reminder.mitraredex.com/assets/user/" + m.getUIMAGE())
                .into(holder.userIvAvatar);

        holder.userTvUsername.setText(m.getUNAME());
        holder.userTvTanggallahir.setText(""+new Helper().convertDateFormat(m.getUTGLLAHIR(), "yyyy-MM-dd"));

        holder.userCvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pref.savePrefString(SharedPref.ID_USER, m.getUBIGID());

                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("data", m);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView userCvMain;
        private CircleImageView userIvAvatar;
        private TextView userTvUsername;
        private TextView userTvTanggallahir;
        public ViewHolder(View itemView) {
            super(itemView);
            userCvMain = itemView.findViewById(R.id.user_cv_main);
            userIvAvatar = itemView.findViewById(R.id.user_iv_avatar);
            userTvUsername = itemView.findViewById(R.id.user_tv_username);
            userTvTanggallahir = itemView.findViewById(R.id.user_tv_tanggallahir);
        }
    }

}