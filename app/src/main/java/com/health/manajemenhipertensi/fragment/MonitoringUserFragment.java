package com.health.manajemenhipertensi.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.health.manajemenhipertensi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MonitoringUserFragment extends Fragment {


    public MonitoringUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_monitoring_user, container, false);
        return view;
    }

}
