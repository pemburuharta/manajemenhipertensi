package com.health.manajemenhipertensi.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.health.manajemenhipertensi.R;

public class AddMateriActivity extends AppCompatActivity {

    private ImageView addmateriIvClose;
    private TextView addmateriTvActionbar;
    private EditText addmateriEdtJudul;
    private LinearLayout addmateriDivSpinner;
    private Spinner addmateriSpinner;
    private EditText addmateriEdtMateri;
    private ImageView addmateriIvThumbnail;
    private TextView addmateriTvThumbnail;
    private LinearLayout addmateriDivSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_materi);
        getSupportActionBar().hide();
        initView();
    }

    private void initView() {
        addmateriIvClose = findViewById(R.id.addmateri_iv_close);
        addmateriTvActionbar = findViewById(R.id.addmateri_tv_actionbar);
        addmateriEdtJudul = findViewById(R.id.addmateri_edt_judul);
        addmateriDivSpinner = findViewById(R.id.addmateri_div_spinner);
        addmateriSpinner = findViewById(R.id.addmateri_spinner);
        addmateriEdtMateri = findViewById(R.id.addmateri_edt_materi);
        addmateriIvThumbnail = findViewById(R.id.addmateri_iv_thumbnail);
        addmateriTvThumbnail = findViewById(R.id.addmateri_tv_thumbnail);
        addmateriDivSimpan = findViewById(R.id.addmateri_div_simpan);
    }
}
