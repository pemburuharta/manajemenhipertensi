package com.health.manajemenhipertensi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterAktifitas;
import com.health.manajemenhipertensi.adapter.AdapterKategori;
import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.model.ModelKategori;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KategoriActivity extends AppCompatActivity {

    private RelativeLayout definisiDivActionbar;
    private ImageView definisiIvClose;
    private TextView definisiTvTitle;
    private SwipeRefreshLayout swipeDefinisi;
    private RecyclerView definisiRv;
    private FloatingActionButton fabAktifitas;
    private RelativeLayout definisiDivNointernet;
    private ImageView definisiIvNointernet;
    private RelativeLayout definisiDivNull;
    private ImageView definisiIvNull;

    SharedPref pref;
    ProgressDialog pd;

    ArrayList<ModelKategori> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategori);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        definisiIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (pref.getRule().equals("USER")){
            fabAktifitas.setVisibility(View.GONE);
        }

        getKategori();

        swipeDefinisi.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getKategori();
            }
        });

    }

    private void getKategori() {

        swipeDefinisi.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getKategori().enqueue(new Callback<ArrayList<ModelKategori>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelKategori>> call, Response<ArrayList<ModelKategori>> response) {
                if (response.isSuccessful()) {
                    swipeDefinisi.setRefreshing(false);
                    dataList = response.body();
                    if (dataList.get(0).getTKBIGID().isEmpty()) {

                    } else {
                        definisiRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        AdapterKategori adapter = new AdapterKategori(getApplicationContext(), dataList);
                        definisiRv.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelKategori>> call, Throwable t) {
                swipeDefinisi.setRefreshing(false);
                Toast.makeText(KategoriActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        definisiDivActionbar = findViewById(R.id.definisi_div_actionbar);
        definisiIvClose = findViewById(R.id.definisi_iv_close);
        definisiTvTitle = findViewById(R.id.definisi_tv_title);
        swipeDefinisi = findViewById(R.id.swipe_definisi);
        definisiRv = findViewById(R.id.definisi_rv);
        fabAktifitas = findViewById(R.id.fab_aktifitas);
        definisiDivNointernet = findViewById(R.id.definisi_div_nointernet);
        definisiIvNointernet = findViewById(R.id.definisi_iv_nointernet);
        definisiDivNull = findViewById(R.id.definisi_div_null);
        definisiIvNull = findViewById(R.id.definisi_iv_null);
    }
}
