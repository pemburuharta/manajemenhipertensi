package com.health.manajemenhipertensi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterDiet;
import com.health.manajemenhipertensi.model.ModelDiet;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DietActivity extends AppCompatActivity {

    private RelativeLayout dietDivActionbar;
    private ImageView dietIvClose;
    private TextView dietTvTitle;
    private SwipeRefreshLayout swipeDiet;
    private RecyclerView dietRv;
    private RelativeLayout dietDivNointernet;
    private ImageView dietIvNointernet;
    private RelativeLayout dietDivNull;
    private ImageView dietIvNull;

    SharedPref pref;
    ProgressDialog pd;
    ArrayList<ModelDiet> dataList = new ArrayList<>();
    String data;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    String judul, ket, tgla, tglb, waktu, cemilan, id;
    private FloatingActionButton fabDiet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        data = getIntent().getStringExtra("data");

        if (data.equals("1")) {
            judul = getIntent().getStringExtra("judul");
            ket = getIntent().getStringExtra("ket");
            tgla = getIntent().getStringExtra("tgla");
            tglb = getIntent().getStringExtra("tglb");
            waktu = getIntent().getStringExtra("waktu");
            cemilan = getIntent().getStringExtra("cemilan");
            id = getIntent().getStringExtra("id");

//            Toast.makeText(this, ""+id+tempat, Toast.LENGTH_SHORT).show();

            tampil(judul, ket, tgla, tglb, waktu, cemilan, id);
        } else {

        }

        if (pref.getRule().equals("USER")){
            fabDiet.setVisibility(View.VISIBLE);
        } else {
            fabDiet.setVisibility(View.GONE);
            fabDiet.setEnabled(false);
        }

        dietIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        fabDiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddPengingatDietActivity.class);
                intent.putExtra("status", "ADD");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        getDiet();
        swipeDiet.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDiet();
            }
        });

    }

    private void getDiet() {

        swipeDiet.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDiet(pref.getIdUser())
                .enqueue(new Callback<ArrayList<ModelDiet>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ModelDiet>> call, Response<ArrayList<ModelDiet>> response) {
                        if (response.isSuccessful()) {
                            swipeDiet.setRefreshing(false);
                            dataList = response.body();
                            if (dataList.get(0).getTHBIGID().isEmpty()) {

                            } else {
                                dietRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                AdapterDiet adapter = new AdapterDiet(getApplicationContext(), dataList);
                                dietRv.setAdapter(adapter);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ModelDiet>> call, Throwable t) {
                        swipeDiet.setRefreshing(false);
                        Toast.makeText(DietActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void tampil(String judul, String ket, String tgla, String tglb, String waktu, String cemilan, final String id) {
        dialog = new AlertDialog.Builder(DietActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.banner_diet, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        ImageView imgOke = dialogView.findViewById(R.id.img_confirm);

        TextView statusServer = (TextView) dialogView.findViewById(R.id.status_server);
        TextView statusText = (TextView) dialogView.findViewById(R.id.status_text);

        EditText adddietEdtWaktu = (EditText) dialogView.findViewById(R.id.adddiet_edt_waktu);
        EditText adddietEdtSayur = (EditText) dialogView.findViewById(R.id.adddiet_edt_sayur);
        EditText adddietEdtTglawal = (EditText) dialogView.findViewById(R.id.adddiet_edt_tglawal);
        EditText adddietEdtTglakhir = (EditText) dialogView.findViewById(R.id.adddiet_edt_tglakhir);

        final AlertDialog alertDialog = dialog.create();

        statusServer.setText(""+judul);
        statusText.setText(""+ket);

        adddietEdtWaktu.setText(""+waktu);
        adddietEdtSayur.setText(""+cemilan);
        adddietEdtTglawal.setText(""+tgla);
        adddietEdtTglakhir.setText(""+tglb);

        adddietEdtWaktu.setFocusable(false);
        adddietEdtSayur.setFocusable(false);
        adddietEdtTglawal.setFocusable(false);
        adddietEdtTglakhir.setFocusable(false);

        imgOke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDiet();
    }

    private void initView() {
        dietDivActionbar = findViewById(R.id.diet_div_actionbar);
        dietIvClose = findViewById(R.id.diet_iv_close);
        dietTvTitle = findViewById(R.id.diet_tv_title);
        swipeDiet = findViewById(R.id.swipe_diet);
        dietRv = findViewById(R.id.diet_rv);
        dietDivNointernet = findViewById(R.id.diet_div_nointernet);
        dietIvNointernet = findViewById(R.id.diet_iv_nointernet);
        dietDivNull = findViewById(R.id.diet_div_null);
        dietIvNull = findViewById(R.id.diet_iv_null);
        fabDiet = findViewById(R.id.fab_diet);
    }
}
