package com.health.manajemenhipertensi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterDiet;
import com.health.manajemenhipertensi.adapter.AdapterUser;
import com.health.manajemenhipertensi.model.ModelUser;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainAdminActivity extends AppCompatActivity {

    SharedPref pref;
    ProgressDialog pd;
    ArrayList<ModelUser> modelUser = new ArrayList<>();
    Calendar calendar;

    private TextView homeTvAdmin;
    private TextView homeTvMorning;
    private TextView homeTvKeluar;
    private CardView homeCvSearch;
    private SwipeRefreshLayout swipeUser;
    private RecyclerView userRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
        calendar = Calendar.getInstance();
        int timeOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        if(timeOfDay >= 0 && timeOfDay < 10){
            homeTvMorning.setText("Selamat Pagi :)");
        }else if(timeOfDay >= 10 && timeOfDay < 16){
            homeTvMorning.setText("Selamat Siang :)");
        }else if(timeOfDay >= 16 && timeOfDay < 18){
            homeTvMorning.setText("Selamat Sore :)");
        }else if(timeOfDay >= 18 && timeOfDay < 24){
            homeTvMorning.setText("Selamat Malam :)");
        }

        homeTvKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.clearAll();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });

        getUser();
        swipeUser.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUser();
            }
        });

    }

    private void getUser() {

        swipeUser.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getUser().enqueue(new Callback<ArrayList<ModelUser>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelUser>> call, Response<ArrayList<ModelUser>> response) {
                if (response.isSuccessful()){
                    modelUser = response.body();
                    swipeUser.setRefreshing(false);
                    if (modelUser.get(0).getUBIGID().isEmpty()) {

                    } else {
                        userRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        AdapterUser adapter= new AdapterUser(getApplicationContext(), modelUser);
                        userRv.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelUser>> call, Throwable t) {
                swipeUser.setRefreshing(false);
                Toast.makeText(MainAdminActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        homeTvAdmin = findViewById(R.id.home_tv_admin);
        homeTvMorning = findViewById(R.id.home_tv_morning);
        homeTvKeluar = findViewById(R.id.home_tv_keluar);
        homeCvSearch = findViewById(R.id.home_cv_search);
        swipeUser = findViewById(R.id.swipe_user);
        userRv = findViewById(R.id.user_rv);
    }
}
