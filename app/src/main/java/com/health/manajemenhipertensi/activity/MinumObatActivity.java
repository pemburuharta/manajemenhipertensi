package com.health.manajemenhipertensi.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notificationbanner.Banner;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterAktifitas;
import com.health.manajemenhipertensi.adapter.AdapterObat;
import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.model.ModelObat;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;
import com.health.manajemenhipertensi.service.ServiceObat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MinumObatActivity extends AppCompatActivity {

    private RelativeLayout minumobatDivActionbar;
    private ImageView minumobatIvClose;
    private TextView minumobatTvTitle;
    private SwipeRefreshLayout swipeMinumobat;
    private RecyclerView minumobatRv;
    private RelativeLayout minumobatDivNointernet;
    private ImageView minumobatIvNointernet;
    private RelativeLayout minumobatDivNull;
    private ImageView minumobatIvNull;
    private FloatingActionButton fabObat;
    View rootview;
    Activity activity;

    SharedPref pref;
    ProgressDialog pd;
    LinearLayout topBar;
    String data,judul,ket,id,nama,dosis,frekuensi,waktu,tersedia,tgla,tglb;;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    int sisa =0;

    ArrayList<ModelObat> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minum_obat);
        getSupportActionBar().hide();
        initView();

        activity = this;

        data = getIntent().getStringExtra("data");

        if (data.equals("1")){

            judul = getIntent().getStringExtra("judul");
            ket = getIntent().getStringExtra("ket");
            id = getIntent().getStringExtra("id");
            nama = getIntent().getStringExtra("nama");
            dosis = getIntent().getStringExtra("dosis");
            frekuensi = getIntent().getStringExtra("frekuensi");
            waktu = getIntent().getStringExtra("waktu");
            tersedia = getIntent().getStringExtra("tersedia");
            tgla = getIntent().getStringExtra("tgla");
            tglb = getIntent().getStringExtra("tglb");

            sisa = Integer.parseInt(tersedia) - Integer.parseInt(frekuensi);

            tampil(judul,ket,id);

        }else {

        }


        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        if (pref.getRule().equals("USER")){
            fabObat.setVisibility(View.VISIBLE);
        } else {
            fabObat.setVisibility(View.GONE);
            fabObat.setEnabled(false);
        }

        minumobatIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        fabObat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddObatActivity.class);
                intent.putExtra("status", "ADD");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        getMinumObat();

        swipeMinumobat.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMinumObat();
            }
        });

    }

    private void tampil(String judul,String ket,String id){
        dialog = new AlertDialog.Builder(MinumObatActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.banner_full, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        Button imgOke = dialogView.findViewById(R.id.img_confirm);
        Button imgCancel = dialogView.findViewById(R.id.img_cancel);
        TextView tvJudul = dialogView.findViewById(R.id.status_server);
        TextView tvKet = dialogView.findViewById(R.id.status_text);

        final AlertDialog alertDialog = dialog.create();

        tvJudul.setText(""+judul);
        tvKet.setText(""+ket);

        imgOke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editObat();
//                Toast.makeText(MinumObatActivity.this, "oke", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MinumObatActivity.this, "cancel", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    private void editObat() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editObat(id,
                nama,
                dosis,
                frekuensi,
                waktu,
                ""+sisa,
                tgla,
                tglb).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    String pesan = jsonObject.optString("msg");
                    if (error.equals("false")){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                        Toast.makeText(MinumObatActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MinumObatActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                new android.app.AlertDialog.Builder(MinumObatActivity.this)
                        .setMessage("Periksa Koneksi Anda...")
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                editObat();
                            }
                        })
                        .show();
//                Toast.makeText(MinumObatActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getMinumObat() {

        swipeMinumobat.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getMinumObat(pref.getIdUser()).enqueue(new Callback<ArrayList<ModelObat>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelObat>> call, Response<ArrayList<ModelObat>> response) {
                if (response.isSuccessful()) {
                    swipeMinumobat.setRefreshing(false);
                    dataList = response.body();
                    if (dataList.get(0).getTMOBIGID().isEmpty()) {

                    } else {
                        minumobatRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        AdapterObat adapter = new AdapterObat(getApplicationContext(), dataList);
                        minumobatRv.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelObat>> call, Throwable t) {
                swipeMinumobat.setRefreshing(false);
                Toast.makeText(MinumObatActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        minumobatDivActionbar = findViewById(R.id.minumobat_div_actionbar);
        minumobatIvClose = findViewById(R.id.minumobat_iv_close);
        minumobatTvTitle = findViewById(R.id.minumobat_tv_title);
        swipeMinumobat = findViewById(R.id.swipe_minumobat);
        minumobatRv = findViewById(R.id.minumobat_rv);
        minumobatDivNointernet = findViewById(R.id.minumobat_div_nointernet);
        minumobatIvNointernet = findViewById(R.id.minumobat_iv_nointernet);
        minumobatDivNull = findViewById(R.id.minumobat_div_null);
        minumobatIvNull = findViewById(R.id.minumobat_iv_null);
        fabObat = findViewById(R.id.fab_obat);
    }
}
