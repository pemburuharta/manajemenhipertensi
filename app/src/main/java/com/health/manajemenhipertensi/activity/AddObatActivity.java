package com.health.manajemenhipertensi.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.model.ModelObat;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddObatActivity extends AppCompatActivity {

    private ImageView addobatIvClose;
    private TextView addobatTvActionbar;
    private EditText addobatEdtNama;
    private EditText addobatEdtDosis;
    private EditText addobatEdtFrekuensi;
    private EditText addobatEdtSedia;
    private EditText addobatEdtWaktu;
    private EditText addobatEdtTglawal;
    private EditText addobatEdtTglakhir;
    private LinearLayout addobatDivSimpan;
    private TimePickerDialog timePickerDialog;

    ModelObat model;
    SharedPref pref;
    ProgressDialog pd;
    Calendar myCalendar;

    String TanggalAwal, TanggalAkhir;
    String NamaObat, Dosis, Sedia, Frekuensi;
    String Status;
    View rootview;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_obat);
        getSupportActionBar().hide();
        initView();

        activity = this;

        Status = getIntent().getStringExtra("status");
        myCalendar = Calendar.getInstance();
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        addobatEdtWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTime();
            }
        });
        addobatEdtTglawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggalAwal();
            }
        });
        addobatEdtTglakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggalAkhir();
            }
        });

        if (Status.equals("ADD")) {

            addobatDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addObat();
                }
            });

        } else {

            model = getIntent().getParcelableExtra("data");

            NamaObat = model.getTMONAMA();
            Dosis = model.getTMODOSIS();
            Frekuensi = model.getTMOFREKUENSI();
            Sedia = model.getTMOKETERSEDIAAN();
            TanggalAwal = model.getTMOSTARTTGL();
            TanggalAkhir = model.getTMOENDTGL();

            addobatEdtNama.setText(NamaObat);
            addobatEdtDosis.setText(Dosis);
            addobatEdtSedia.setText(Sedia);
            addobatEdtFrekuensi.setText(Frekuensi);
            addobatEdtWaktu.setText(model.getTMOWAKTU());
            addobatEdtTglawal.setText(model.getTMOSTARTTGL());
            addobatEdtTglakhir.setText(model.getTMOENDTGL());

            addobatDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editObat();
                }
            });

        }

    }

    private void editObat() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editObat(model.getTMOBIGID(),
                addobatEdtNama.getText().toString(),
                addobatEdtDosis.getText().toString(),
                addobatEdtFrekuensi.getText().toString(),
                addobatEdtWaktu.getText().toString(),
                addobatEdtSedia.getText().toString(),
                TanggalAwal,
                TanggalAkhir).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    String pesan = jsonObject.optString("msg");
                    if (error.equals("false")){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                        Toast.makeText(AddObatActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddObatActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddObatActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addObat() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addObat(pref.getIdUser(),
                addobatEdtNama.getText().toString(),
                addobatEdtDosis.getText().toString(),
                addobatEdtFrekuensi.getText().toString(),
                addobatEdtWaktu.getText().toString(),
                addobatEdtSedia.getText().toString(),
                TanggalAwal,
                TanggalAkhir).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")) {

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            Toast.makeText(AddObatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(AddObatActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddObatActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void dialogTime() {

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                addobatEdtWaktu.setText(i + ":" + i1);
            }
        },
                myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(this));
        timePickerDialog.show();

    }

    private void tanggalAkhir() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                addobatEdtTglakhir.setText(dateFormat.format(myCalendar.getTime()));

                TanggalAkhir = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void tanggalAwal() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                addobatEdtTglawal.setText(dateFormat.format(myCalendar.getTime()));

                TanggalAwal = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void initView() {
        addobatIvClose = findViewById(R.id.addobat_iv_close);
        addobatTvActionbar = findViewById(R.id.addobat_tv_actionbar);
        addobatEdtNama = findViewById(R.id.addobat_edt_nama);
        addobatEdtDosis = findViewById(R.id.addobat_edt_dosis);
        addobatEdtFrekuensi = findViewById(R.id.addobat_edt_frekuensi);
        addobatEdtSedia = findViewById(R.id.addobat_edt_sedia);
        addobatEdtWaktu = findViewById(R.id.addobat_edt_waktu);
        addobatEdtTglawal = findViewById(R.id.addobat_edt_tglawal);
        addobatEdtTglakhir = findViewById(R.id.addobat_edt_tglakhir);
        addobatDivSimpan = findViewById(R.id.addobat_div_simpan);
    }
}
