package com.health.manajemenhipertensi.activity;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;

import com.health.manajemenhipertensi.R;

public class StopWatchActivity extends AppCompatActivity {

    private ImageView imgPlay;
    private ImageView imgPause;
    private ImageView imgStop;
    private boolean running;
    private long nilauPause;
    private Chronometer chronometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_watch);
        initView();

        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play();
            }
        });

        imgPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pause();
            }
        });

        imgStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stop();
            }
        });
    }

    private void play() {
        if (!running) {
            chronometer.setBase(SystemClock.elapsedRealtime() - nilauPause);
            chronometer.start();
            running = true;
        }
    }

    private void pause() {
        if (running){
            chronometer.stop();
            nilauPause = SystemClock.elapsedRealtime() - chronometer.getBase();
            running = false;
        }
    }

    private void stop() {
        chronometer.setBase(SystemClock.elapsedRealtime());
        nilauPause = 0;
    }

    private void initView() {
        imgPlay = (ImageView) findViewById(R.id.img_play);
        imgPause = (ImageView) findViewById(R.id.img_pause);
        imgStop = (ImageView) findViewById(R.id.img_stop);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
    }
}
