package com.health.manajemenhipertensi.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAktivitasActivity extends AppCompatActivity {

    private ImageView addaktivitasIvClose;
    private TextView addaktivitasTvActionbar;
    private EditText addaktivitasEdtWaktu;
    private Spinner addaktivitasSpinnerAktifitas;
    private EditText addaktivitasEdtTglawal;
    private EditText addaktivitasEdtTglakhir;
    private LinearLayout addaktivitasDivSimpan;
    private TextView tvAktifitas;
    private TimePickerDialog timePickerDialog;

    ModelAktifitas model;
    SharedPref pref;
    ProgressDialog pd;
    Calendar myCalendar;

    String TanggalAwal, TanggalAkhir;
    String Aktifitas;
    String Status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_aktivitas);
        getSupportActionBar().hide();
        initView();

        Status = getIntent().getStringExtra("status");
        myCalendar = Calendar.getInstance();
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        setAktifitas();
        addaktivitasEdtWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTime();
            }
        });
        addaktivitasEdtTglawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggalAwal();
            }
        });
        addaktivitasEdtTglakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggalAkhir();
            }
        });

        if (Status.equals("ADD")) {

            addaktivitasDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addAktifitas();
                }
            });

        } else {

            model = getIntent().getParcelableExtra("data");

            Aktifitas = model.getTAJENIS();
            TanggalAwal = model.getTASTARTTGL();
            TanggalAkhir = model.getTAENDTGL();

            tvAktifitas.setText(Aktifitas);
            addaktivitasEdtWaktu.setText(model.getTAWAKTU());
            addaktivitasEdtTglawal.setText(model.getTASTARTTGL());
            addaktivitasEdtTglakhir.setText(model.getTAENDTGL());

            addaktivitasDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editAktifitas();
                }
            });

        }

    }

    private void editAktifitas() {
        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editAktifitas(model.getTABIGID(),
                addaktivitasEdtWaktu.getText().toString(),
                Aktifitas,
                TanggalAwal,
                TanggalAkhir).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")){
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            Toast.makeText(AddAktivitasActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AddAktivitasActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddAktivitasActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addAktifitas() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addAktifitas(pref.getIdUser(),
                addaktivitasEdtWaktu.getText().toString(),
                Aktifitas,
                TanggalAwal,
                TanggalAkhir).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")) {

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            Toast.makeText(AddAktivitasActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(AddAktivitasActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddAktivitasActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setAktifitas() {

        final List<String> listAktifitas = new ArrayList<>();
        listAktifitas.add("Pilih Aktifitas");
        listAktifitas.add("Jalan Kaki");
        listAktifitas.add("Bersepeda");
        listAktifitas.add("Olahraga");
        listAktifitas.add("Tidak Beraktifitas");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listAktifitas);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addaktivitasSpinnerAktifitas.setAdapter(adapter);
        addaktivitasSpinnerAktifitas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int a = addaktivitasSpinnerAktifitas.getSelectedItemPosition();

                if (a != 0) {
                    Aktifitas = listAktifitas.get(a);
                    tvAktifitas.setText(Aktifitas);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void dialogTime() {

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                addaktivitasEdtWaktu.setText(i + ":" + i1);
            }
        },
                myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(this));
        timePickerDialog.show();

    }

    private void tanggalAkhir() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                addaktivitasEdtTglakhir.setText(dateFormat.format(myCalendar.getTime()));

                TanggalAkhir = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void tanggalAwal() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                addaktivitasEdtTglawal.setText(dateFormat.format(myCalendar.getTime()));

                TanggalAwal = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void initView() {
        addaktivitasIvClose = findViewById(R.id.addaktivitas_iv_close);
        addaktivitasTvActionbar = findViewById(R.id.addaktivitas_tv_actionbar);
        addaktivitasEdtWaktu = findViewById(R.id.addaktivitas_edt_waktu);
        addaktivitasSpinnerAktifitas = findViewById(R.id.addaktivitas_spinner_aktifitas);
        addaktivitasEdtTglawal = findViewById(R.id.addaktivitas_edt_tglawal);
        addaktivitasEdtTglakhir = findViewById(R.id.addaktivitas_edt_tglakhir);
        addaktivitasDivSimpan = findViewById(R.id.addaktivitas_div_simpan);
        tvAktifitas = findViewById(R.id.tv_aktifitas);
    }
}
