package com.health.manajemenhipertensi.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.Helper;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private EditText registerEdtUsername;
    private EditText registerEdtPassword;
    private LinearLayout registerDivDaftar;
    private TextView registerTvLogin;
    private EditText registerEdtNamalengkap;
    private EditText registerEdtTgllahir;
    private RadioButton registerRbLaki;
    private RadioButton registerRbPerempuan;

    String Gender;
    String Tanggal;

    SharedPref pref;
    ProgressDialog pd;
    Calendar myCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        myCalendar = Calendar.getInstance();

        registerEdtTgllahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggal();
            }
        });

        registerDivDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (registerRbLaki.isChecked()){
                    Gender = "LK";
                } else if (registerRbPerempuan.isChecked()){
                    Gender = "PR";
                }
//
                register();
//                Toast.makeText(RegisterActivity.this, ""+registerEdtUsername.getText().toString(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(RegisterActivity.this, ""+ registerEdtNamalengkap.getText().toString(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(RegisterActivity.this, ""+ registerEdtTgllahir.getText().toString(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(RegisterActivity.this, ""+ Gender, Toast.LENGTH_SHORT).show();
//                Toast.makeText(RegisterActivity.this, ""+ registerEdtPassword.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        registerTvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void tanggal() {

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                registerEdtTgllahir.setText(""+new Helper().convertDateFormat(dateFormat.format(myCalendar.getTime()), "yyyy-MM-dd"));

                Tanggal = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    private void register() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.register(registerEdtUsername.getText().toString(),
                registerEdtNamalengkap.getText().toString(),
                registerEdtTgllahir.getText().toString(),
                Gender,
                registerEdtPassword.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")){

                            String id_user = jsonObject.getString("bigId");
                            String name = jsonObject.getString("name");
                            String fullname = jsonObject.getString("fullname");
                            String tgl_lahir = jsonObject.getString("tgl_lahir");
                            String gender = jsonObject.getString("gender");
                            String image = jsonObject.getString("image");
                            String pekerjaan = jsonObject.getString("pekerjaan");
                            String alamat = jsonObject.getString("alamat");
                            String rule = jsonObject.getString("rule");

                            pref.savePrefBoolean(SharedPref.STATUS_LOGIN, true);
                            pref.savePrefString(SharedPref.ID_USER, id_user);
                            pref.savePrefString(SharedPref.NAME, name);
                            pref.savePrefString(SharedPref.FULLNAME, fullname);
                            pref.savePrefString(SharedPref.TANGGAL_LAHIR, tgl_lahir);
                            pref.savePrefString(SharedPref.GENDER, gender);
                            pref.savePrefString(SharedPref.AVATAR, image);
                            pref.savePrefString(SharedPref.PEKERJAAN, pekerjaan);
                            pref.savePrefString(SharedPref.ALAMAT, alamat);
                            pref.savePrefString(SharedPref.RULE, rule);

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        } else {

                            Toast.makeText(RegisterActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(RegisterActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();

            }
        });

    }


    private void initView() {
        registerEdtUsername = findViewById(R.id.register_edt_username);
        registerEdtPassword = findViewById(R.id.register_edt_password);
        registerDivDaftar = findViewById(R.id.register_div_daftar);
        registerTvLogin = findViewById(R.id.register_tv_login);
        registerEdtNamalengkap = findViewById(R.id.register_edt_namalengkap);
        registerEdtTgllahir = findViewById(R.id.register_edt_tgllahir);
        registerRbLaki = findViewById(R.id.register_rb_laki);
        registerRbPerempuan = findViewById(R.id.register_rb_perempuan);
    }
}
