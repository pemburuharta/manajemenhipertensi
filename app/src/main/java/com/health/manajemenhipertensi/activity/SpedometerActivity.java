package com.health.manajemenhipertensi.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.health.manajemenhipertensi.R;

public class SpedometerActivity extends AppCompatActivity implements LocationListener {

    private TextView tvSpedo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spedometer);
        initView();

        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        this.onLocationChanged(null);

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            tvSpedo.setText("-,- m/s");
        }else {
            float sped = location.getSpeed();
            tvSpedo.setText(""+sped + " m/s");
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void initView() {
        tvSpedo = (TextView) findViewById(R.id.tv_spedo);
    }
}
