package com.health.manajemenhipertensi.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterMateri;
import com.health.manajemenhipertensi.model.ModelMateri;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MateriActivity extends AppCompatActivity {

    private RelativeLayout materiDivActionbar;
    private ImageView materiIvClose;
    private TextView materiTvTitle;
    private SwipeRefreshLayout swipeMateri;
    private FloatingActionButton fabMateri;
    private RelativeLayout materiDivNointernet;
    private ImageView materiIvNointernet;
    private RelativeLayout materiDivNull;
    private ImageView materiIvNull;

    SharedPref pref;
    ProgressDialog pd;

    String idKategori;

    ArrayList<ModelMateri> dataList = new ArrayList<>();
    private ImageView image;
    private TextView tvJudul;
    private TextView tvIsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        idKategori = getIntent().getStringExtra("id");

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        materiIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (pref.getRule().equals("USER")) {
            fabMateri.setVisibility(View.GONE);
        }

        getMateri();

        swipeMateri.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMateri();
            }
        });

    }

    private void getMateri() {

        swipeMateri.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getMateri(idKategori).enqueue(new Callback<ArrayList<ModelMateri>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelMateri>> call, Response<ArrayList<ModelMateri>> response) {
                if (response.isSuccessful()) {
                    swipeMateri.setRefreshing(false);
                    dataList = response.body();
                    if (dataList.size() == 0) {

                    } else {
                        tvIsi.setText(dataList.get(0).getTMISI());
                        tvJudul.setText(dataList.get(0).getTMJUDUL());
                        Picasso.with(getApplicationContext())
                                .load("http://reminder.mitraredex.com/assets/kategori/"+dataList.get(0).getTMGAMBAR())
                                .into(image);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelMateri>> call, Throwable t) {
                swipeMateri.setRefreshing(false);
                Toast.makeText(MateriActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        materiDivActionbar = findViewById(R.id.materi_div_actionbar);
        materiIvClose = findViewById(R.id.materi_iv_close);
        materiTvTitle = findViewById(R.id.materi_tv_title);
        swipeMateri = findViewById(R.id.swipe_materi);
        fabMateri = findViewById(R.id.fab_materi);
        materiDivNointernet = findViewById(R.id.materi_div_nointernet);
        materiIvNointernet = findViewById(R.id.materi_iv_nointernet);
        materiDivNull = findViewById(R.id.materi_div_null);
        materiIvNull = findViewById(R.id.materi_iv_null);
        image = (ImageView) findViewById(R.id.image);
        tvJudul = (TextView) findViewById(R.id.tv_judul);
        tvIsi = (TextView) findViewById(R.id.tv_isi);
    }
}
