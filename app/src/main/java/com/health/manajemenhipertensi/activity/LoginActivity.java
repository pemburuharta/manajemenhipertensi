package com.health.manajemenhipertensi.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText loginEdtUsername;
    private EditText loginEdtPassword;
    private LinearLayout loginDivMasuk;
    private TextView loginTvRegister;

    SharedPref pref;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        initView();

        Permission();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        if (pref.getStatusLogin()){

            if (pref.getRule().equals("USER")){

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } else {
                Intent intent = new Intent(getApplicationContext(), MainAdminActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        }

        loginDivMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        loginTvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    private void login() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.Login(loginEdtUsername.getText().toString(),
                loginEdtPassword.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")){
                            String id_user = jsonObject.getString("bigId");
                            String name = jsonObject.getString("name");
                            String fullname = jsonObject.getString("fullname");
                            String tgl_lahir = jsonObject.getString("tgl_lahir");
                            String gender = jsonObject.getString("gender");
                            String image = jsonObject.getString("image");
                            String pekerjaan = jsonObject.getString("pekerjaan");
                            String alamat = jsonObject.getString("alamat");
                            String rule = jsonObject.getString("rule");

                            pref.savePrefBoolean(SharedPref.STATUS_LOGIN, true);
                            pref.savePrefString(SharedPref.NAME, name);
                            pref.savePrefString(SharedPref.FULLNAME, fullname);
                            pref.savePrefString(SharedPref.TANGGAL_LAHIR, tgl_lahir);
                            pref.savePrefString(SharedPref.GENDER, gender);
                            pref.savePrefString(SharedPref.AVATAR, image);
                            pref.savePrefString(SharedPref.PEKERJAAN, pekerjaan);
                            pref.savePrefString(SharedPref.ALAMAT, alamat);
                            pref.savePrefString(SharedPref.RULE, rule);

                            if (rule.equals("USER")){

                                pref.savePrefString(SharedPref.ID_USER, id_user);

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }else {
                                Intent intent = new Intent(getApplicationContext(), MainAdminActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }


                        }else {
                            pd.dismiss();
                            Toast.makeText(LoginActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void Permission() {
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(LoginActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private void initView() {
        loginEdtUsername = findViewById(R.id.login_edt_username);
        loginEdtPassword = findViewById(R.id.login_edt_password);
        loginDivMasuk = findViewById(R.id.login_div_masuk);
        loginTvRegister = findViewById(R.id.login_tv_register);
    }
}
