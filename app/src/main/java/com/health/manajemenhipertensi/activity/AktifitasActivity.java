package com.health.manajemenhipertensi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterAktifitas;
import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AktifitasActivity extends AppCompatActivity {


    private RelativeLayout aktifitasDivActionbar;
    private ImageView aktifitasIvClose;
    private TextView aktifitasTvTitle;
    private SwipeRefreshLayout swipeAktifitas;
    private RecyclerView aktifitasRv;
    private RelativeLayout aktifitasDivNointernet;
    private ImageView aktifitasIvNointernet;
    private RelativeLayout aktifitasDivNull;
    private ImageView aktifitasIvNull;

    SharedPref pref;
    ProgressDialog pd;
    String data,judul,ket,id;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    ArrayList<ModelAktifitas> dataList = new ArrayList<>();
    private FloatingActionButton fabAktifitas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aktifitas);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);

        data = getIntent().getStringExtra("data");

        if (data.equals("1")){
            judul = getIntent().getStringExtra("judul");
            ket = getIntent().getStringExtra("ket");
            id = getIntent().getStringExtra("id");

            tampil(judul,ket,id);

        }else {

        }

        if (pref.getRule().equals("USER")){
            fabAktifitas.setVisibility(View.VISIBLE);
        } else {
            fabAktifitas.setVisibility(View.GONE);
            fabAktifitas.setEnabled(false);
        }

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        aktifitasIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        fabAktifitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddAktivitasActivity.class);
                intent.putExtra("status", "ADD");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        getAktifitas();
        swipeAktifitas.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAktifitas();
            }
        });

    }

    private void getAktifitas() {

        swipeAktifitas.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getAktifitas(pref.getIdUser())
                .enqueue(new Callback<ArrayList<ModelAktifitas>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ModelAktifitas>> call, Response<ArrayList<ModelAktifitas>> response) {
                        if (response.isSuccessful()) {
                            swipeAktifitas.setRefreshing(false);
                            dataList = response.body();
                            if (dataList.get(0).getTABIGID().isEmpty()) {

                            } else {
                                aktifitasRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                AdapterAktifitas adapter = new AdapterAktifitas(getApplicationContext(), dataList);
                                aktifitasRv.setAdapter(adapter);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ModelAktifitas>> call, Throwable t) {
                        swipeAktifitas.setRefreshing(false);
                        Toast.makeText(AktifitasActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void tampil(String judul,String ket,String id){
        dialog = new AlertDialog.Builder(AktifitasActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.banner_aktifitas, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        Button imgOke = dialogView.findViewById(R.id.img_confirm);
        Button imgCancel = dialogView.findViewById(R.id.img_cancel);
        TextView tvJudul = dialogView.findViewById(R.id.status_server);
        TextView tvKet = dialogView.findViewById(R.id.status_text);

        final AlertDialog alertDialog = dialog.create();

        tvJudul.setText(""+judul);
        tvKet.setText(""+ket);

        imgOke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tampilDua();
                alertDialog.dismiss();
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AktifitasActivity.this, "cancel", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void tampilDua(){
        dialog = new AlertDialog.Builder(AktifitasActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.banner_aktifitas_dua, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        Button btnSatu = dialogView.findViewById(R.id.btn_spedo);
        Button btnDua = dialogView.findViewById(R.id.btn_stopwatch);


        final AlertDialog alertDialog = dialog.create();


        btnSatu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),SpedometerActivity.class));

            }
        });

        btnDua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),StopWatchActivity.class));
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAktifitas();
    }

    private void initView() {
        aktifitasDivActionbar = findViewById(R.id.aktifitas_div_actionbar);
        aktifitasIvClose = findViewById(R.id.aktifitas_iv_close);
        aktifitasTvTitle = findViewById(R.id.aktifitas_tv_title);
        swipeAktifitas = findViewById(R.id.swipe_aktifitas);
        aktifitasRv = findViewById(R.id.aktifitas_rv);
        aktifitasDivNointernet = findViewById(R.id.aktifitas_div_nointernet);
        aktifitasIvNointernet = findViewById(R.id.aktifitas_iv_nointernet);
        aktifitasDivNull = findViewById(R.id.aktifitas_div_null);
        aktifitasIvNull = findViewById(R.id.aktifitas_iv_null);
        fabAktifitas = findViewById(R.id.fab_aktifitas);
    }
}
