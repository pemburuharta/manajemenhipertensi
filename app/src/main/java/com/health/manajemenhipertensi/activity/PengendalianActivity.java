package com.health.manajemenhipertensi.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterKategori;
import com.health.manajemenhipertensi.adapter.AdapterPengendalian;
import com.health.manajemenhipertensi.model.ModelKategori;
import com.health.manajemenhipertensi.model.ModelPengendalian;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengendalianActivity extends AppCompatActivity {

    private RelativeLayout pengendalianDivActionbar;
    private ImageView pengendalianIvClose;
    private TextView pengendalianTvTitle;
    private SwipeRefreshLayout swipePengendalian;
    private RecyclerView pengendalianRv;
    private FloatingActionButton fabPengendalian;
    private RelativeLayout pengendalianDivNointernet;
    private ImageView pengendalianIvNointernet;
    private RelativeLayout pengendalianDivNull;
    private ImageView pengendalianIvNull;

    SharedPref pref;
    ProgressDialog pd;

    ArrayList<ModelPengendalian> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengendalian);
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        pengendalianIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (pref.getRule().equals("USER")){
            fabPengendalian.setVisibility(View.GONE);
        }

        getPengendalian();

        swipePengendalian.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPengendalian();
            }
        });

    }

    private void getPengendalian() {

        swipePengendalian.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getPengendalian().enqueue(new Callback<ArrayList<ModelPengendalian>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelPengendalian>> call, Response<ArrayList<ModelPengendalian>> response) {
                if (response.isSuccessful()) {
                    swipePengendalian.setRefreshing(false);
                    dataList = response.body();
                    if (dataList.get(0).getTRBIGID().isEmpty()) {

                    } else {
                        pengendalianRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        AdapterPengendalian adapter = new AdapterPengendalian(getApplicationContext(), dataList);
                        pengendalianRv.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelPengendalian>> call, Throwable t) {
                swipePengendalian.setRefreshing(false);
                Toast.makeText(PengendalianActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        pengendalianDivActionbar = findViewById(R.id.pengendalian_div_actionbar);
        pengendalianIvClose = findViewById(R.id.pengendalian_iv_close);
        pengendalianTvTitle = findViewById(R.id.pengendalian_tv_title);
        swipePengendalian = findViewById(R.id.swipe_pengendalian);
        pengendalianRv = findViewById(R.id.pengendalian_rv);
        fabPengendalian = findViewById(R.id.fab_pengendalian);
        pengendalianDivNointernet = findViewById(R.id.pengendalian_div_nointernet);
        pengendalianIvNointernet = findViewById(R.id.pengendalian_iv_nointernet);
        pengendalianDivNull = findViewById(R.id.pengendalian_div_null);
        pengendalianIvNull = findViewById(R.id.pengendalian_iv_null);
    }
}
