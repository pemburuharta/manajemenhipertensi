package com.health.manajemenhipertensi.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilActivity extends AppCompatActivity {

    private ImageView identitasIvClose;
    private TextView identitasTvActionbar;
    private CircleImageView identitasIvAvatar;
    private TextView identitasTvUsername;
    private EditText identitasEdtNama;
    private EditText identitasEdtTgllahir;
    private EditText identitasEdtAlamat;
    private RadioButton identitasRbPria;
    private RadioButton identitasEdtPerempuan;
    private EditText identitasEdtPekerjaan;
    private LinearLayout identitasDivSimpan;

    Calendar myCalendar;
    SharedPref pref;
    ProgressDialog pd;

    String TanggalLahir;
    String Foto;
    String Gender;
    File imageFile;
    private LinearLayout identitasDivKeluar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        getSupportActionBar().hide();
        initView();

        myCalendar = Calendar.getInstance();
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        getData();

        identitasIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfil();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        identitasDivKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pref.savePrefBoolean(pref.STATUS_LOGIN, false);
                pref.savePrefString(SharedPref.ID_USER, "");
                pref.savePrefString(SharedPref.NAME, "");
                pref.savePrefString(SharedPref.FULLNAME, "");
                pref.savePrefString(SharedPref.TANGGAL_LAHIR, "");
                pref.savePrefString(SharedPref.GENDER, "");
                pref.savePrefString(SharedPref.AVATAR, "");
                pref.savePrefString(SharedPref.PEKERJAAN, "");
                pref.savePrefString(SharedPref.ALAMAT, "");
                pref.savePrefString(SharedPref.RULE, "");
                startActivity(new Intent(getApplicationContext(), LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
        identitasEdtTgllahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ProfilActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        myCalendar.set(Calendar.YEAR, i);
                        myCalendar.set(Calendar.MONTH, i1);
                        myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                        String fromTanggal = "yyyy-MM-dd";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                        identitasEdtTgllahir.setText(dateFormat.format(myCalendar.getTime()));

                        TanggalLahir = dateFormat.format(myCalendar.getTime());
                    }
                },
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        identitasIvAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openGallery(ProfilActivity.this, 3);
            }
        });

        identitasDivSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (identitasRbPria.isChecked()) {
                    Gender = "LK";
                } else if (identitasEdtPerempuan.isChecked()) {
                    Gender = "PR";
                }

                updateProfil();
            }
        });

    }

    private void getData() {

        Gender = pref.getGender();
        Foto = pref.getAvatar();
        TanggalLahir = pref.getTanggalLahir();

        if (Gender.equals("LK")) {
            identitasRbPria.setChecked(true);
        } else {
            identitasEdtPerempuan.setChecked(true);
        }

        identitasTvUsername.setText(pref.getName());
        identitasEdtNama.setText(pref.getFullname());
        identitasEdtTgllahir.setText(pref.getTanggalLahir());
        identitasEdtAlamat.setText(pref.getAlamat());
        identitasEdtPekerjaan.setText(pref.getPekerjaan());

        if (Foto != null) {

            Picasso.with(this)
                    .load("http://reminder.mitraredex.com/assets/user/" + Foto)
                    .into(identitasIvAvatar);

        } else {
            identitasIvAvatar.setImageResource(R.drawable.ic_profil);
        }

    }

    private void updateProfil() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editProfil(pref.getIdUser(),
                identitasEdtNama.getText().toString(),
                TanggalLahir,
                Gender,
                identitasEdtPekerjaan.getText().toString(),
                identitasEdtAlamat.getText().toString(),
                Foto).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")) {

                            pref.savePrefString(SharedPref.AVATAR, Foto);
                            pref.savePrefString(SharedPref.FULLNAME, identitasEdtNama.getText().toString());
                            pref.savePrefString(SharedPref.TANGGAL_LAHIR, TanggalLahir);
                            pref.savePrefString(SharedPref.GENDER, Gender);
                            pref.savePrefString(SharedPref.PEKERJAAN, identitasEdtPekerjaan.getText().toString());
                            pref.savePrefString(SharedPref.ALAMAT, identitasEdtAlamat.getText().toString());
                            pref.savePrefString(SharedPref.AVATAR, Foto);

                            onBackPressed();

                            Toast.makeText(ProfilActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ProfilActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ProfilActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(ProfilActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File image, EasyImage.ImageSource source, int type) {
                Picasso.with(ProfilActivity.this)
                        .load(new File(image.getPath()))
                        .into(identitasIvAvatar);
                imageFile = new File(image.getPath());
                uploadGambar();
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                super.onCanceled(source, type);
            }

        });
    }

    private void uploadGambar() {

        RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part bodyGambar = MultipartBody.Part.createFormData("image", imageFile.getName(), requestImage);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.imageProfil(bodyGambar).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    String pesan = jsonObject.optString("msg");
                    String nama = jsonObject.optString("image_name");
                    if (error.equals("false")) {

                        Foto = nama;
                        pref.savePrefString(SharedPref.AVATAR, Foto);


                    } else {
                        Toast.makeText(ProfilActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void initView() {
        identitasIvClose = findViewById(R.id.identitas_iv_close);
        identitasTvActionbar = findViewById(R.id.identitas_tv_actionbar);
        identitasIvAvatar = findViewById(R.id.identitas_iv_avatar);
        identitasTvUsername = findViewById(R.id.identitas_tv_username);
        identitasEdtNama = findViewById(R.id.identitas_edt_nama);
        identitasEdtTgllahir = findViewById(R.id.identitas_edt_tgllahir);
        identitasEdtAlamat = findViewById(R.id.identitas_edt_alamat);
        identitasRbPria = findViewById(R.id.identitas_rb_pria);
        identitasEdtPerempuan = findViewById(R.id.identitas_edt_perempuan);
        identitasEdtPekerjaan = findViewById(R.id.identitas_edt_pekerjaan);
        identitasDivSimpan = findViewById(R.id.identitas_div_simpan);
        identitasDivKeluar = (LinearLayout) findViewById(R.id.identitas_div_keluar);
    }

}
