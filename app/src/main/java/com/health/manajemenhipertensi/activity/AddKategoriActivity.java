package com.health.manajemenhipertensi.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.health.manajemenhipertensi.R;

public class AddKategoriActivity extends AppCompatActivity {

    private ImageView addkategoriIvClose;
    private TextView addkategoriTvActionbar;
    private EditText addkategoriEdtNama;
    private ImageView addkategoriIvThumbnail;
    private TextView addkategoriTvThumbnail;
    private LinearLayout addkategoriDivSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kategori);
        getSupportActionBar().hide();
        initView();



    }

    private void initView() {
        addkategoriIvClose = findViewById(R.id.addkategori_iv_close);
        addkategoriTvActionbar = findViewById(R.id.addkategori_tv_actionbar);
        addkategoriEdtNama = findViewById(R.id.addkategori_edt_nama);
        addkategoriIvThumbnail = findViewById(R.id.addkategori_iv_thumbnail);
        addkategoriTvThumbnail = findViewById(R.id.addkategori_tv_thumbnail);
        addkategoriDivSimpan = findViewById(R.id.addkategori_div_simpan);
    }
}
