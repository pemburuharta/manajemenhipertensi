package com.health.manajemenhipertensi.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.adapter.AdapterCekKesehatan;
import com.health.manajemenhipertensi.model.ModelCekKesehatan;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CekKesehatanActivity extends AppCompatActivity {

    private RelativeLayout cekkesehatanDivActionbar;
    private ImageView cekkesehatanIvClose;
    private TextView cekkesehatanTvTitle;
    private SwipeRefreshLayout swipeCekkesehatan;
    private RecyclerView cekkesehatanRv;
    private RelativeLayout cekkesehatanDivNointernet;
    private ImageView cekkesehatanIvNointernet;
    private RelativeLayout cekkesehatanDivNull;
    private ImageView cekkesehatanIvNull;

    SharedPref pref;
    ProgressDialog pd;
    String data;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    String judul, ket, tgl, waktu, tempat, id;

    ArrayList<ModelCekKesehatan> dataList = new ArrayList<>();
    private FloatingActionButton fabKesehatan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cek_kesehatan);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);

        data = getIntent().getStringExtra("data");

        if (data.equals("1")) {
            judul = getIntent().getStringExtra("judul");
            ket = getIntent().getStringExtra("ket");
            tgl = getIntent().getStringExtra("tgl");
            waktu = getIntent().getStringExtra("waktu");
            tempat = getIntent().getStringExtra("tempat");
            id = getIntent().getStringExtra("id");

//            Toast.makeText(this, ""+id+tempat, Toast.LENGTH_SHORT).show();

            tampil(judul, ket, tgl, waktu, tempat, id);
        } else {

        }

        if (pref.getRule().equals("USER")){
            fabKesehatan.setVisibility(View.VISIBLE);
        } else {
            fabKesehatan.setVisibility(View.GONE);
            fabKesehatan.setEnabled(false);
        }

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        cekkesehatanIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        fabKesehatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddCekKesehatanActivity.class);
                intent.putExtra("status", "ADD");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        getCekKesehatan();
        swipeCekkesehatan.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCekKesehatan();
            }
        });

    }

    private void getCekKesehatan() {

        swipeCekkesehatan.setRefreshing(true);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getCekKesehatan(
                pref.getIdUser()).enqueue(new Callback<ArrayList<ModelCekKesehatan>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelCekKesehatan>> call, Response<ArrayList<ModelCekKesehatan>> response) {
                if (response.isSuccessful()) {
                    swipeCekkesehatan.setRefreshing(false);
                    dataList = response.body();
                    if (dataList.get(0).getTKBIGID().isEmpty()) {

                    } else {
                        cekkesehatanRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        AdapterCekKesehatan adapter = new AdapterCekKesehatan(getApplicationContext(), dataList);
                        cekkesehatanRv.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelCekKesehatan>> call, Throwable t) {
                swipeCekkesehatan.setRefreshing(false);
                Toast.makeText(CekKesehatanActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void tampil(String judul, String ket, String tgl, String waktu, String tempat, final String id) {
        dialog = new AlertDialog.Builder(CekKesehatanActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.banner_kesehatan, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        ImageView imgOke = dialogView.findViewById(R.id.img_confirm);
        TextView tvJudul = dialogView.findViewById(R.id.status_server);
        TextView tvKet = dialogView.findViewById(R.id.status_text);
        EditText edtWaktu = dialogView.findViewById(R.id.edt_waktu);
        EditText edtTanggal = dialogView.findViewById(R.id.edt_tglawal);
        EditText edtTempat = dialogView.findViewById(R.id.edt_tempat);

        final AlertDialog alertDialog = dialog.create();

        tvJudul.setText("" + judul);
        tvKet.setText("" + ket);
        edtWaktu.setText("" + waktu);
        edtTanggal.setText("" + tgl);
        edtTempat.setText("" + tempat);

        imgOke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapus();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void hapus() {
        final ProgressDialog pd;
        pd = new ProgressDialog(CekKesehatanActivity.this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.deleteKesehatan(
                "delete",
                id)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            String error = jsonObject.optString("error");
                            if (error.equals("false")) {
                                pd.dismiss();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                Toast.makeText(CekKesehatanActivity.this, "Pengingat Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                            } else {
                                pd.dismiss();
                                Toast.makeText(CekKesehatanActivity.this, "Pengingat Gagal Dihapus", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        pd.dismiss();
                        new android.app.AlertDialog.Builder(CekKesehatanActivity.this)
                                .setMessage("" + t.getMessage())
                                .setCancelable(false)
                                .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                     hapus();
                                    }
                                })
                                .show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCekKesehatan();
    }

    private void initView() {
        cekkesehatanDivActionbar = findViewById(R.id.cekkesehatan_div_actionbar);
        cekkesehatanIvClose = findViewById(R.id.cekkesehatan_iv_close);
        cekkesehatanTvTitle = findViewById(R.id.cekkesehatan_tv_title);
        swipeCekkesehatan = findViewById(R.id.swipe_cekkesehatan);
        cekkesehatanRv = findViewById(R.id.cekkesehatan_rv);
        cekkesehatanDivNointernet = findViewById(R.id.cekkesehatan_div_nointernet);
        cekkesehatanIvNointernet = findViewById(R.id.cekkesehatan_iv_nointernet);
        cekkesehatanDivNull = findViewById(R.id.cekkesehatan_div_null);
        cekkesehatanIvNull = findViewById(R.id.cekkesehatan_iv_null);
        fabKesehatan = findViewById(R.id.fab_kesehatan);
    }
}
