package com.health.manajemenhipertensi.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.health.manajemenhipertensi.R;

public class AddPengendalianActivity extends AppCompatActivity {

    private ImageView addpengendalianIvClose;
    private TextView addpengendalianTvActionbar;
    private EditText addpengendalianEdtJudul;
    private EditText addpengendalianEdtMateri;
    private ImageView addpengendalianIvThumbnail;
    private TextView addpengendalianTvThumbnail;
    private LinearLayout addpengendalianDivSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pengendalian);
        getSupportActionBar().hide();
        initView();
    }

    private void initView() {
        addpengendalianIvClose = findViewById(R.id.addpengendalian_iv_close);
        addpengendalianTvActionbar = findViewById(R.id.addpengendalian_tv_actionbar);
        addpengendalianEdtJudul = findViewById(R.id.addpengendalian_edt_judul);
        addpengendalianEdtMateri = findViewById(R.id.addpengendalian_edt_materi);
        addpengendalianIvThumbnail = findViewById(R.id.addpengendalian_iv_thumbnail);
        addpengendalianTvThumbnail = findViewById(R.id.addpengendalian_tv_thumbnail);
        addpengendalianDivSimpan = findViewById(R.id.addpengendalian_div_simpan);
    }
}
