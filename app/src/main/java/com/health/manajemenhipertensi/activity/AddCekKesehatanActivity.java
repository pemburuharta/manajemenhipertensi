package com.health.manajemenhipertensi.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.model.ModelCekKesehatan;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCekKesehatanActivity extends AppCompatActivity {

    private ImageView addcekkesehatanIvClose;
    private TextView addcekkesehatanTvActionbar;
    private EditText addcekkesehatanEdtWaktu;
    private EditText addcekkesehatanEdtTglawal;
    private EditText addcekkesehatanEdtTempat;
    private LinearLayout addcekkesehatanDivSimpan;
    private TimePickerDialog timePickerDialog;

    SharedPref pref;
    ProgressDialog pd;
    Calendar myCalendar;
    ModelCekKesehatan model;

    String TanggalAwal, TanggalAkhir;
    String Status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cek_kesehatan);
        getSupportActionBar().hide();
        initView();

        Status = getIntent().getStringExtra("status");
        myCalendar = Calendar.getInstance();
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        addcekkesehatanEdtTglawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggalAwal();
            }
        });
        addcekkesehatanEdtWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTime();
            }
        });

        if (Status.equals("ADD")){

            addcekkesehatanDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addCekKesehatan();
                }
            });

        } else {

            model = getIntent().getParcelableExtra("data");

            TanggalAwal = model.getTKTGL();
            addcekkesehatanEdtWaktu.setText(model.getTKWAKTU());
            addcekkesehatanEdtTglawal.setText(model.getTKTGL());
            addcekkesehatanEdtTempat.setText(model.getTKTEMPAT());

            addcekkesehatanDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    editCekKesehatan();
                }
            });

        }


    }

    private void editCekKesehatan() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editKesehatan(model.getTKBIGID(),
                addcekkesehatanEdtWaktu.getText().toString(),
                addcekkesehatanEdtTempat.getText().toString(),
                TanggalAwal).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    String pesan = jsonObject.optString("msg");
                    if (error.equals("false")){
//                        onBackPressed();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                        Toast.makeText(AddCekKesehatanActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddCekKesehatanActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddCekKesehatanActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addCekKesehatan() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addKesehatan(pref.getIdUser(),
                addcekkesehatanEdtWaktu.getText().toString(),
                addcekkesehatanEdtTempat.getText().toString(),
                TanggalAwal).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")){

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();

                            Toast.makeText(AddCekKesehatanActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(AddCekKesehatanActivity.this, ""+pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddCekKesehatanActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void dialogTime() {

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                addcekkesehatanEdtWaktu.setText(i+":"+i1);
            }
        },
                myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    private void tanggalAwal() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                addcekkesehatanEdtTglawal.setText(dateFormat.format(myCalendar.getTime()));

                TanggalAwal = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void initView() {
        addcekkesehatanIvClose = findViewById(R.id.addcekkesehatan_iv_close);
        addcekkesehatanTvActionbar = findViewById(R.id.addcekkesehatan_tv_actionbar);
        addcekkesehatanEdtWaktu = findViewById(R.id.addcekkesehatan_edt_waktu);
        addcekkesehatanEdtTglawal = findViewById(R.id.addcekkesehatan_edt_tglawal);
        addcekkesehatanEdtTempat = findViewById(R.id.addcekkesehatan_edt_tempat);
        addcekkesehatanDivSimpan = findViewById(R.id.addcekkesehatan_div_simpan);
    }
}
