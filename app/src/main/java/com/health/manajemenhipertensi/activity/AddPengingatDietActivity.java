package com.health.manajemenhipertensi.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.MainActivity;
import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.model.ModelDiet;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPengingatDietActivity extends AppCompatActivity {

    private ImageView adddietIvClose;
    private TextView adddietTvActionbar;
    private EditText adddietEdtWaktu;
    private Spinner adddietSpinnerSayuran;
    private EditText adddietEdtTglawal;
    private EditText adddietEdtTglakhir;
    private TextView tvSayuran;
    private LinearLayout adddietDivSimpan;
    private TimePickerDialog timePickerDialog;

    ModelDiet model;
    SharedPref pref;
    ProgressDialog pd;
    Calendar myCalendar;

    String TanggalAwal, TanggalAkhir, Makanan;
    String Status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pengingat_diet);
        getSupportActionBar().hide();
        initView();

        Status = getIntent().getStringExtra("status");
        myCalendar = Calendar.getInstance();
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);


        setMakanan();
        adddietEdtWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTime();
            }
        });
        adddietEdtTglawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggalAwal();
            }
        });
        adddietEdtTglakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tanggalAkhir();
            }
        });

        if (Status.equals("ADD")) {

            adddietDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AddDiet();
                }
            });

        } else {

            model = getIntent().getParcelableExtra("data");

            Makanan = model.getTHCEMILAN();
            TanggalAwal = model.getTHSTARTTGL();
            TanggalAkhir = model.getTHENDTGL();

            tvSayuran.setText(Makanan);
            adddietEdtWaktu.setText(model.getTHWAKTU());
            adddietEdtTglawal.setText(model.getTHSTARTTGL());
            adddietEdtTglakhir.setText(model.getTHENDTGL());

            adddietDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editDiet();
                }
            });

        }

    }

    private void editDiet() {
        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.editDiet(model.getTHBIGID(),
                adddietEdtWaktu.getText().toString(),
                Makanan,
                TanggalAwal,
                TanggalAkhir).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    String pesan = jsonObject.optString("msg");
                    if (error.equals("false")) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                        Toast.makeText(AddPengingatDietActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddPengingatDietActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddPengingatDietActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void AddDiet() {

        pd.show();

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.addDiet(pref.getIdUser(),
                adddietEdtWaktu.getText().toString(),
                Makanan,
                TanggalAwal,
                TanggalAkhir).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String pesan = jsonObject.optString("msg");
                        if (error.equals("false")) {

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            Toast.makeText(AddPengingatDietActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(AddPengingatDietActivity.this, "" + pesan, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(AddPengingatDietActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void tanggalAkhir() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                adddietEdtTglakhir.setText(dateFormat.format(myCalendar.getTime()));

                TanggalAkhir = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void tanggalAwal() {
        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String fromTanggal = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                adddietEdtTglawal.setText(dateFormat.format(myCalendar.getTime()));

                TanggalAwal = dateFormat.format(myCalendar.getTime());
            }
        },
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void setMakanan() {

        final List<String> listMakanan = new ArrayList<>();
        listMakanan.add("Pilih Makanan");
        listMakanan.add("Sayuran");
        listMakanan.add("Buah");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddPengingatDietActivity.this, android.R.layout.simple_list_item_1, listMakanan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adddietSpinnerSayuran.setAdapter(adapter);
        adddietSpinnerSayuran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int a = adddietSpinnerSayuran.getSelectedItemPosition();

                if (a != 0) {
                    Makanan = listMakanan.get(a);
                    tvSayuran.setText(Makanan);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void dialogTime() {

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                adddietEdtWaktu.setText(i + ":" + i1);
            }
        },
                myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(this));
        timePickerDialog.show();

    }

    private void initView() {
        adddietIvClose = findViewById(R.id.adddiet_iv_close);
        adddietTvActionbar = findViewById(R.id.adddiet_tv_actionbar);
        adddietEdtWaktu = findViewById(R.id.adddiet_edt_waktu);
        adddietSpinnerSayuran = findViewById(R.id.adddiet_spinner_sayuran);
        adddietEdtTglawal = findViewById(R.id.adddiet_edt_tglawal);
        adddietEdtTglakhir = findViewById(R.id.adddiet_edt_tglakhir);
        adddietDivSimpan = findViewById(R.id.adddiet_div_simpan);
        tvSayuran = findViewById(R.id.tv_sayuran);
    }
}
