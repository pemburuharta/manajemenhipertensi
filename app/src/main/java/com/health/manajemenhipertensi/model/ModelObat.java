package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelObat implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("TMO_BIGID")
    @Expose
    private String tMOBIGID;
    @SerializedName("TMO_USERID")
    @Expose
    private String tMOUSERID;
    @SerializedName("TMO_NAMA")
    @Expose
    private String tMONAMA;
    @SerializedName("TMO_DOSIS")
    @Expose
    private String tMODOSIS;
    @SerializedName("TMO_FREKUENSI")
    @Expose
    private String tMOFREKUENSI;
    @SerializedName("TMO_WAKTU")
    @Expose
    private String tMOWAKTU;
    @SerializedName("TMO_START_TGL")
    @Expose
    private String tMOSTARTTGL;
    @SerializedName("TMO_END_TGL")
    @Expose
    private String tMOENDTGL;
    @SerializedName("TMO_KETERSEDIAAN")
    @Expose
    private String tMOKETERSEDIAAN;
    @SerializedName("TMO_STATUS")
    @Expose
    private String tMOSTATUS;
    @SerializedName("TMO_CREATED_AT")
    @Expose
    private String tMOCREATEDAT;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTMOBIGID() {
        return tMOBIGID;
    }

    public void setTMOBIGID(String tMOBIGID) {
        this.tMOBIGID = tMOBIGID;
    }

    public String getTMOUSERID() {
        return tMOUSERID;
    }

    public void setTMOUSERID(String tMOUSERID) {
        this.tMOUSERID = tMOUSERID;
    }

    public String getTMONAMA() {
        return tMONAMA;
    }

    public void setTMONAMA(String tMONAMA) {
        this.tMONAMA = tMONAMA;
    }

    public String getTMODOSIS() {
        return tMODOSIS;
    }

    public void setTMODOSIS(String tMODOSIS) {
        this.tMODOSIS = tMODOSIS;
    }

    public String getTMOFREKUENSI() {
        return tMOFREKUENSI;
    }

    public void setTMOFREKUENSI(String tMOFREKUENSI) {
        this.tMOFREKUENSI = tMOFREKUENSI;
    }

    public String getTMOWAKTU() {
        return tMOWAKTU;
    }

    public void setTMOWAKTU(String tMOWAKTU) {
        this.tMOWAKTU = tMOWAKTU;
    }

    public String getTMOSTARTTGL() {
        return tMOSTARTTGL;
    }

    public void setTMOSTARTTGL(String tMOSTARTTGL) {
        this.tMOSTARTTGL = tMOSTARTTGL;
    }

    public String getTMOENDTGL() {
        return tMOENDTGL;
    }

    public void setTMOENDTGL(String tMOENDTGL) {
        this.tMOENDTGL = tMOENDTGL;
    }

    public String getTMOKETERSEDIAAN() {
        return tMOKETERSEDIAAN;
    }

    public void setTMOKETERSEDIAAN(String tMOKETERSEDIAAN) {
        this.tMOKETERSEDIAAN = tMOKETERSEDIAAN;
    }

    public String getTMOSTATUS() {
        return tMOSTATUS;
    }

    public void setTMOSTATUS(String tMOSTATUS) {
        this.tMOSTATUS = tMOSTATUS;
    }

    public String getTMOCREATEDAT() {
        return tMOCREATEDAT;
    }

    public void setTMOCREATEDAT(String tMOCREATEDAT) {
        this.tMOCREATEDAT = tMOCREATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.error);
        dest.writeValue(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.tMOBIGID);
        dest.writeString(this.tMOUSERID);
        dest.writeString(this.tMONAMA);
        dest.writeString(this.tMODOSIS);
        dest.writeString(this.tMOFREKUENSI);
        dest.writeString(this.tMOWAKTU);
        dest.writeString(this.tMOSTARTTGL);
        dest.writeString(this.tMOENDTGL);
        dest.writeString(this.tMOKETERSEDIAAN);
        dest.writeString(this.tMOSTATUS);
        dest.writeString(this.tMOCREATEDAT);
    }

    public ModelObat() {
    }

    protected ModelObat(Parcel in) {
        this.error = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.msg = in.readString();
        this.tMOBIGID = in.readString();
        this.tMOUSERID = in.readString();
        this.tMONAMA = in.readString();
        this.tMODOSIS = in.readString();
        this.tMOFREKUENSI = in.readString();
        this.tMOWAKTU = in.readString();
        this.tMOSTARTTGL = in.readString();
        this.tMOENDTGL = in.readString();
        this.tMOKETERSEDIAAN = in.readString();
        this.tMOSTATUS = in.readString();
        this.tMOCREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelObat> CREATOR = new Parcelable.Creator<ModelObat>() {
        @Override
        public ModelObat createFromParcel(Parcel source) {
            return new ModelObat(source);
        }

        @Override
        public ModelObat[] newArray(int size) {
            return new ModelObat[size];
        }
    };
}
