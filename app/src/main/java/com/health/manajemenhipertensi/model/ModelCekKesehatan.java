package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelCekKesehatan implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("TK_BIGID")
    @Expose
    private String tKBIGID;
    @SerializedName("TK_USERID")
    @Expose
    private String tKUSERID;
    @SerializedName("TK_WAKTU")
    @Expose
    private String tKWAKTU;
    @SerializedName("TK_TGL")
    @Expose
    private String tKTGL;
    @SerializedName("TK_TEMPAT")
    @Expose
    private String tKTEMPAT;
    @SerializedName("TK_CREATED_AT\t")
    @Expose
    private String tKCREATEDAT;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTKBIGID() {
        return tKBIGID;
    }

    public void setTKBIGID(String tKBIGID) {
        this.tKBIGID = tKBIGID;
    }

    public String getTKUSERID() {
        return tKUSERID;
    }

    public void setTKUSERID(String tKUSERID) {
        this.tKUSERID = tKUSERID;
    }

    public String getTKWAKTU() {
        return tKWAKTU;
    }

    public void setTKWAKTU(String tKWAKTU) {
        this.tKWAKTU = tKWAKTU;
    }

    public String getTKTGL() {
        return tKTGL;
    }

    public void setTKTGL(String tKTGL) {
        this.tKTGL = tKTGL;
    }

    public String getTKTEMPAT() {
        return tKTEMPAT;
    }

    public void setTKTEMPAT(String tKTEMPAT) {
        this.tKTEMPAT = tKTEMPAT;
    }

    public String getTKCREATEDAT() {
        return tKCREATEDAT;
    }

    public void setTKCREATEDAT(String tKCREATEDAT) {
        this.tKCREATEDAT = tKCREATEDAT;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.error);
        dest.writeValue(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.tKBIGID);
        dest.writeString(this.tKUSERID);
        dest.writeString(this.tKWAKTU);
        dest.writeString(this.tKTGL);
        dest.writeString(this.tKTEMPAT);
        dest.writeString(this.tKCREATEDAT);
    }

    public ModelCekKesehatan() {
    }

    protected ModelCekKesehatan(Parcel in) {
        this.error = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.msg = in.readString();
        this.tKBIGID = in.readString();
        this.tKUSERID = in.readString();
        this.tKWAKTU = in.readString();
        this.tKTGL = in.readString();
        this.tKTEMPAT = in.readString();
        this.tKCREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelCekKesehatan> CREATOR = new Parcelable.Creator<ModelCekKesehatan>() {
        @Override
        public ModelCekKesehatan createFromParcel(Parcel source) {
            return new ModelCekKesehatan(source);
        }

        @Override
        public ModelCekKesehatan[] newArray(int size) {
            return new ModelCekKesehatan[size];
        }
    };
}
