package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelPengendalian implements Parcelable {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("TR_BIGID")
    @Expose
    private String tRBIGID;
    @SerializedName("TR_NAMA")
    @Expose
    private String tRNAMA;
    @SerializedName("TR_ISI")
    @Expose
    private String tRISI;
    @SerializedName("TR_IMAGE")
    @Expose
    private String tRIMAGE;
    @SerializedName("TR_CREATED_AT")
    @Expose
    private String tRCREATEDAT;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTRBIGID() {
        return tRBIGID;
    }

    public void setTRBIGID(String tRBIGID) {
        this.tRBIGID = tRBIGID;
    }

    public String getTRNAMA() {
        return tRNAMA;
    }

    public void setTRNAMA(String tRNAMA) {
        this.tRNAMA = tRNAMA;
    }

    public String getTRISI() {
        return tRISI;
    }

    public void setTRISI(String tRISI) {
        this.tRISI = tRISI;
    }

    public String getTRIMAGE() {
        return tRIMAGE;
    }

    public void setTRIMAGE(String tRIMAGE) {
        this.tRIMAGE = tRIMAGE;
    }

    public String getTRCREATEDAT() {
        return tRCREATEDAT;
    }

    public void setTRCREATEDAT(String tRCREATEDAT) {
        this.tRCREATEDAT = tRCREATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.error);
        dest.writeValue(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.tRBIGID);
        dest.writeString(this.tRNAMA);
        dest.writeString(this.tRISI);
        dest.writeString(this.tRIMAGE);
        dest.writeString(this.tRCREATEDAT);
    }

    public ModelPengendalian() {
    }

    protected ModelPengendalian(Parcel in) {
        this.error = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.msg = in.readString();
        this.tRBIGID = in.readString();
        this.tRNAMA = in.readString();
        this.tRISI = in.readString();
        this.tRIMAGE = in.readString();
        this.tRCREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelPengendalian> CREATOR = new Parcelable.Creator<ModelPengendalian>() {
        @Override
        public ModelPengendalian createFromParcel(Parcel source) {
            return new ModelPengendalian(source);
        }

        @Override
        public ModelPengendalian[] newArray(int size) {
            return new ModelPengendalian[size];
        }
    };
}
