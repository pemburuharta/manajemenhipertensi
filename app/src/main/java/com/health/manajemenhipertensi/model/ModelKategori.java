package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelKategori implements Parcelable {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("TK_BIGID")
    @Expose
    private String tKBIGID;
    @SerializedName("TK_NAMA")
    @Expose
    private String tKNAMA;
    @SerializedName("TK_IMAGE")
    @Expose
    private String tKIMAGE;
    @SerializedName("TK_CREATED_AT")
    @Expose
    private String tKCREATEDAT;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTKBIGID() {
        return tKBIGID;
    }

    public void setTKBIGID(String tKBIGID) {
        this.tKBIGID = tKBIGID;
    }

    public String getTKNAMA() {
        return tKNAMA;
    }

    public void setTKNAMA(String tKNAMA) {
        this.tKNAMA = tKNAMA;
    }

    public String getTKIMAGE() {
        return tKIMAGE;
    }

    public void setTKIMAGE(String tKIMAGE) {
        this.tKIMAGE = tKIMAGE;
    }

    public String getTKCREATEDAT() {
        return tKCREATEDAT;
    }

    public void setTKCREATEDAT(String tKCREATEDAT) {
        this.tKCREATEDAT = tKCREATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.error);
        dest.writeValue(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.tKBIGID);
        dest.writeString(this.tKNAMA);
        dest.writeString(this.tKIMAGE);
        dest.writeString(this.tKCREATEDAT);
    }

    public ModelKategori() {
    }

    protected ModelKategori(Parcel in) {
        this.error = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.msg = in.readString();
        this.tKBIGID = in.readString();
        this.tKNAMA = in.readString();
        this.tKIMAGE = in.readString();
        this.tKCREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelKategori> CREATOR = new Parcelable.Creator<ModelKategori>() {
        @Override
        public ModelKategori createFromParcel(Parcel source) {
            return new ModelKategori(source);
        }

        @Override
        public ModelKategori[] newArray(int size) {
            return new ModelKategori[size];
        }
    };
}
