package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelMateri implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("TM_BIGID")
    @Expose
    private String tMBIGID;
    @SerializedName("TM_KATID")
    @Expose
    private String tMKATID;
    @SerializedName("TM_JUDUL")
    @Expose
    private String tMJUDUL;
    @SerializedName("TM_ISI")
    @Expose
    private String tMISI;
    @SerializedName("TM_GAMBAR")
    @Expose
    private String tMGAMBAR;
    @SerializedName("TM_CREATED_AT")
    @Expose
    private String tMCREATEDAT;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTMBIGID() {
        return tMBIGID;
    }

    public void setTMBIGID(String tMBIGID) {
        this.tMBIGID = tMBIGID;
    }

    public String getTMKATID() {
        return tMKATID;
    }

    public void setTMKATID(String tMKATID) {
        this.tMKATID = tMKATID;
    }

    public String getTMJUDUL() {
        return tMJUDUL;
    }

    public void setTMJUDUL(String tMJUDUL) {
        this.tMJUDUL = tMJUDUL;
    }

    public String getTMISI() {
        return tMISI;
    }

    public void setTMISI(String tMISI) {
        this.tMISI = tMISI;
    }

    public String getTMGAMBAR() {
        return tMGAMBAR;
    }

    public void setTMGAMBAR(String tMGAMBAR) {
        this.tMGAMBAR = tMGAMBAR;
    }

    public String getTMCREATEDAT() {
        return tMCREATEDAT;
    }

    public void setTMCREATEDAT(String tMCREATEDAT) {
        this.tMCREATEDAT = tMCREATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.error);
        dest.writeValue(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.tMBIGID);
        dest.writeString(this.tMKATID);
        dest.writeString(this.tMJUDUL);
        dest.writeString(this.tMISI);
        dest.writeString(this.tMGAMBAR);
        dest.writeString(this.tMCREATEDAT);
    }

    public ModelMateri() {
    }

    protected ModelMateri(Parcel in) {
        this.error = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.msg = in.readString();
        this.tMBIGID = in.readString();
        this.tMKATID = in.readString();
        this.tMJUDUL = in.readString();
        this.tMISI = in.readString();
        this.tMGAMBAR = in.readString();
        this.tMCREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelMateri> CREATOR = new Parcelable.Creator<ModelMateri>() {
        @Override
        public ModelMateri createFromParcel(Parcel source) {
            return new ModelMateri(source);
        }

        @Override
        public ModelMateri[] newArray(int size) {
            return new ModelMateri[size];
        }
    };
}
