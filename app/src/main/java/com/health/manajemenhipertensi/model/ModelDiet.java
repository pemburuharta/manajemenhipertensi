package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelDiet implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("TH_BIGID")
    @Expose
    private String tHBIGID;
    @SerializedName("TH_USERID")
    @Expose
    private String tHUSERID;
    @SerializedName("TH_WAKTU")
    @Expose
    private String tHWAKTU;
    @SerializedName("TH_CEMILAN")
    @Expose
    private String tHCEMILAN;
    @SerializedName("TH_START_TGL")
    @Expose
    private String tHSTARTTGL;
    @SerializedName("TH_END_TGL")
    @Expose
    private String tHENDTGL;
    @SerializedName("TH_STATUS")
    @Expose
    private String tHSTATUS;
    @SerializedName("TH_CREATED_AT")
    @Expose
    private String tHCREATEDAT;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTHBIGID() {
        return tHBIGID;
    }

    public void setTHBIGID(String tHBIGID) {
        this.tHBIGID = tHBIGID;
    }

    public String getTHUSERID() {
        return tHUSERID;
    }

    public void setTHUSERID(String tHUSERID) {
        this.tHUSERID = tHUSERID;
    }

    public String getTHWAKTU() {
        return tHWAKTU;
    }

    public void setTHWAKTU(String tHWAKTU) {
        this.tHWAKTU = tHWAKTU;
    }

    public String getTHCEMILAN() {
        return tHCEMILAN;
    }

    public void setTHCEMILAN(String tHCEMILAN) {
        this.tHCEMILAN = tHCEMILAN;
    }

    public String getTHSTARTTGL() {
        return tHSTARTTGL;
    }

    public void setTHSTARTTGL(String tHSTARTTGL) {
        this.tHSTARTTGL = tHSTARTTGL;
    }

    public String getTHENDTGL() {
        return tHENDTGL;
    }

    public void setTHENDTGL(String tHENDTGL) {
        this.tHENDTGL = tHENDTGL;
    }

    public String getTHSTATUS() {
        return tHSTATUS;
    }

    public void setTHSTATUS(String tHSTATUS) {
        this.tHSTATUS = tHSTATUS;
    }

    public String getTHCREATEDAT() {
        return tHCREATEDAT;
    }

    public void setTHCREATEDAT(String tHCREATEDAT) {
        this.tHCREATEDAT = tHCREATEDAT;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.error);
        dest.writeValue(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.tHBIGID);
        dest.writeString(this.tHUSERID);
        dest.writeString(this.tHWAKTU);
        dest.writeString(this.tHCEMILAN);
        dest.writeString(this.tHSTARTTGL);
        dest.writeString(this.tHENDTGL);
        dest.writeString(this.tHSTATUS);
        dest.writeString(this.tHCREATEDAT);
    }

    public ModelDiet() {
    }

    protected ModelDiet(Parcel in) {
        this.error = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.msg = in.readString();
        this.tHBIGID = in.readString();
        this.tHUSERID = in.readString();
        this.tHWAKTU = in.readString();
        this.tHCEMILAN = in.readString();
        this.tHSTARTTGL = in.readString();
        this.tHENDTGL = in.readString();
        this.tHSTATUS = in.readString();
        this.tHCREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelDiet> CREATOR = new Parcelable.Creator<ModelDiet>() {
        @Override
        public ModelDiet createFromParcel(Parcel source) {
            return new ModelDiet(source);
        }

        @Override
        public ModelDiet[] newArray(int size) {
            return new ModelDiet[size];
        }
    };
}
