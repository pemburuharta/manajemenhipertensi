package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelUser implements Parcelable {

    @SerializedName("U_BIGID")
    @Expose
    private String uBIGID;
    @SerializedName("U_NAME")
    @Expose
    private String uNAME;
    @SerializedName("U_FULLNAME")
    @Expose
    private String uFULLNAME;
    @SerializedName("U_TGL_LAHIR")
    @Expose
    private String uTGLLAHIR;
    @SerializedName("U_JK")
    @Expose
    private String uJK;
    @SerializedName("U_IMAGE")
    @Expose
    private String uIMAGE;
    @SerializedName("U_PEKERJAAN")
    @Expose
    private String uPEKERJAAN;
    @SerializedName("U_ALAMAT")
    @Expose
    private String uALAMAT;
    @SerializedName("U_PASSWORD")
    @Expose
    private String uPASSWORD;
    @SerializedName("U_STATUS")
    @Expose
    private String uSTATUS;
    @SerializedName("U_GROUP_RULE")
    @Expose
    private String uGROUPRULE;
    @SerializedName("U_LOGIN_TOKEN")
    @Expose
    private String uLOGINTOKEN;
    @SerializedName("U_LOGIN_WAKTU")
    @Expose
    private String uLOGINWAKTU;
    @SerializedName("U_DEFAULT_BROWSER")
    @Expose
    private String uDEFAULTBROWSER;
    @SerializedName("U_IP_POSITION")
    @Expose
    private String uIPPOSITION;
    @SerializedName("U_CREATED_AT")
    @Expose
    private String uCREATEDAT;

    public String getUBIGID() {
        return uBIGID;
    }

    public void setUBIGID(String uBIGID) {
        this.uBIGID = uBIGID;
    }

    public String getUNAME() {
        return uNAME;
    }

    public void setUNAME(String uNAME) {
        this.uNAME = uNAME;
    }

    public String getUFULLNAME() {
        return uFULLNAME;
    }

    public void setUFULLNAME(String uFULLNAME) {
        this.uFULLNAME = uFULLNAME;
    }

    public String getUTGLLAHIR() {
        return uTGLLAHIR;
    }

    public void setUTGLLAHIR(String uTGLLAHIR) {
        this.uTGLLAHIR = uTGLLAHIR;
    }

    public String getUJK() {
        return uJK;
    }

    public void setUJK(String uJK) {
        this.uJK = uJK;
    }

    public String getUIMAGE() {
        return uIMAGE;
    }

    public void setUIMAGE(String uIMAGE) {
        this.uIMAGE = uIMAGE;
    }

    public String getUPEKERJAAN() {
        return uPEKERJAAN;
    }

    public void setUPEKERJAAN(String uPEKERJAAN) {
        this.uPEKERJAAN = uPEKERJAAN;
    }

    public String getUALAMAT() {
        return uALAMAT;
    }

    public void setUALAMAT(String uALAMAT) {
        this.uALAMAT = uALAMAT;
    }

    public String getUPASSWORD() {
        return uPASSWORD;
    }

    public void setUPASSWORD(String uPASSWORD) {
        this.uPASSWORD = uPASSWORD;
    }

    public String getUSTATUS() {
        return uSTATUS;
    }

    public void setUSTATUS(String uSTATUS) {
        this.uSTATUS = uSTATUS;
    }

    public String getUGROUPRULE() {
        return uGROUPRULE;
    }

    public void setUGROUPRULE(String uGROUPRULE) {
        this.uGROUPRULE = uGROUPRULE;
    }

    public String getULOGINTOKEN() {
        return uLOGINTOKEN;
    }

    public void setULOGINTOKEN(String uLOGINTOKEN) {
        this.uLOGINTOKEN = uLOGINTOKEN;
    }

    public String getULOGINWAKTU() {
        return uLOGINWAKTU;
    }

    public void setULOGINWAKTU(String uLOGINWAKTU) {
        this.uLOGINWAKTU = uLOGINWAKTU;
    }

    public String getUDEFAULTBROWSER() {
        return uDEFAULTBROWSER;
    }

    public void setUDEFAULTBROWSER(String uDEFAULTBROWSER) {
        this.uDEFAULTBROWSER = uDEFAULTBROWSER;
    }

    public String getUIPPOSITION() {
        return uIPPOSITION;
    }

    public void setUIPPOSITION(String uIPPOSITION) {
        this.uIPPOSITION = uIPPOSITION;
    }

    public String getUCREATEDAT() {
        return uCREATEDAT;
    }

    public void setUCREATEDAT(String uCREATEDAT) {
        this.uCREATEDAT = uCREATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uBIGID);
        dest.writeString(this.uNAME);
        dest.writeString(this.uFULLNAME);
        dest.writeString(this.uTGLLAHIR);
        dest.writeString(this.uJK);
        dest.writeString(this.uIMAGE);
        dest.writeString(this.uPEKERJAAN);
        dest.writeString(this.uALAMAT);
        dest.writeString(this.uPASSWORD);
        dest.writeString(this.uSTATUS);
        dest.writeString(this.uGROUPRULE);
        dest.writeString(this.uLOGINTOKEN);
        dest.writeString(this.uLOGINWAKTU);
        dest.writeString(this.uDEFAULTBROWSER);
        dest.writeString(this.uIPPOSITION);
        dest.writeString(this.uCREATEDAT);
    }

    public ModelUser() {
    }

    protected ModelUser(Parcel in) {
        this.uBIGID = in.readString();
        this.uNAME = in.readString();
        this.uFULLNAME = in.readString();
        this.uTGLLAHIR = in.readString();
        this.uJK = in.readString();
        this.uIMAGE = in.readString();
        this.uPEKERJAAN = in.readString();
        this.uALAMAT = in.readString();
        this.uPASSWORD = in.readString();
        this.uSTATUS = in.readString();
        this.uGROUPRULE = in.readString();
        this.uLOGINTOKEN = in.readString();
        this.uLOGINWAKTU = in.readString();
        this.uDEFAULTBROWSER = in.readString();
        this.uIPPOSITION = in.readString();
        this.uCREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelUser> CREATOR = new Parcelable.Creator<ModelUser>() {
        @Override
        public ModelUser createFromParcel(Parcel source) {
            return new ModelUser(source);
        }

        @Override
        public ModelUser[] newArray(int size) {
            return new ModelUser[size];
        }
    };
}
