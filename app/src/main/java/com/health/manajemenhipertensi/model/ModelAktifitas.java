package com.health.manajemenhipertensi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelAktifitas implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("TA_BIGID")
    @Expose
    private String tABIGID;
    @SerializedName("TA_USERID")
    @Expose
    private String tAUSERID;
    @SerializedName("TA_WAKTU")
    @Expose
    private String tAWAKTU;
    @SerializedName("TA_JENIS")
    @Expose
    private String tAJENIS;
    @SerializedName("TA_START_TGL")
    @Expose
    private String tASTARTTGL;
    @SerializedName("TA_END_TGL")
    @Expose
    private String tAENDTGL;
    @SerializedName("TA_STATUS")
    @Expose
    private String tASTATUS;
    @SerializedName("TA_CREATED_AT")
    @Expose
    private String tACREATEDAT;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTABIGID() {
        return tABIGID;
    }

    public void setTABIGID(String tABIGID) {
        this.tABIGID = tABIGID;
    }

    public String getTAUSERID() {
        return tAUSERID;
    }

    public void setTAUSERID(String tAUSERID) {
        this.tAUSERID = tAUSERID;
    }

    public String getTAWAKTU() {
        return tAWAKTU;
    }

    public void setTAWAKTU(String tAWAKTU) {
        this.tAWAKTU = tAWAKTU;
    }

    public String getTAJENIS() {
        return tAJENIS;
    }

    public void setTAJENIS(String tAJENIS) {
        this.tAJENIS = tAJENIS;
    }

    public String getTASTARTTGL() {
        return tASTARTTGL;
    }

    public void setTASTARTTGL(String tASTARTTGL) {
        this.tASTARTTGL = tASTARTTGL;
    }

    public String getTAENDTGL() {
        return tAENDTGL;
    }

    public void setTAENDTGL(String tAENDTGL) {
        this.tAENDTGL = tAENDTGL;
    }

    public String getTASTATUS() {
        return tASTATUS;
    }

    public void setTASTATUS(String tASTATUS) {
        this.tASTATUS = tASTATUS;
    }

    public String getTACREATEDAT() {
        return tACREATEDAT;
    }

    public void setTACREATEDAT(String tACREATEDAT) {
        this.tACREATEDAT = tACREATEDAT;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.error);
        dest.writeValue(this.status);
        dest.writeString(this.msg);
        dest.writeString(this.tABIGID);
        dest.writeString(this.tAUSERID);
        dest.writeString(this.tAWAKTU);
        dest.writeString(this.tAJENIS);
        dest.writeString(this.tASTARTTGL);
        dest.writeString(this.tAENDTGL);
        dest.writeString(this.tASTATUS);
        dest.writeString(this.tACREATEDAT);
    }

    public ModelAktifitas() {
    }

    protected ModelAktifitas(Parcel in) {
        this.error = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.msg = in.readString();
        this.tABIGID = in.readString();
        this.tAUSERID = in.readString();
        this.tAWAKTU = in.readString();
        this.tAJENIS = in.readString();
        this.tASTARTTGL = in.readString();
        this.tAENDTGL = in.readString();
        this.tASTATUS = in.readString();
        this.tACREATEDAT = in.readString();
    }

    public static final Parcelable.Creator<ModelAktifitas> CREATOR = new Parcelable.Creator<ModelAktifitas>() {
        @Override
        public ModelAktifitas createFromParcel(Parcel source) {
            return new ModelAktifitas(source);
        }

        @Override
        public ModelAktifitas[] newArray(int size) {
            return new ModelAktifitas[size];
        }
    };
}
