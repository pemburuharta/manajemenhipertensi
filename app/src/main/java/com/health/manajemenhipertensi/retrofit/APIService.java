package com.health.manajemenhipertensi.retrofit;

import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.model.ModelCekKesehatan;
import com.health.manajemenhipertensi.model.ModelDiet;
import com.health.manajemenhipertensi.model.ModelKategori;
import com.health.manajemenhipertensi.model.ModelMateri;
import com.health.manajemenhipertensi.model.ModelObat;
import com.health.manajemenhipertensi.model.ModelPengendalian;
import com.health.manajemenhipertensi.model.ModelUser;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {

    @FormUrlEncoded
    @POST("authentifikasi?acces=login")
    Call<ResponseBody> Login(
            @Field("loginName") String loginName,
            @Field("loginPassword") String loginPassword
    );

    @FormUrlEncoded
    @POST("authentifikasi?acces=register")
    Call<ResponseBody> register(
            @Field("name") String name,
            @Field("fullname") String fullname,
            @Field("tgl_lahir") String tgl_lahir,
            @Field("gender") String gender,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("profile-api?acces=ubah")
    Call<ResponseBody> editProfil(
            @Query("value") String value,
            @Field("fullname") String fullname,
            @Field("tgl_lahir") String tgl_lahir,
            @Field("gender") String gender,
            @Field("pekerjaan") String pekerjaan,
            @Field("alamat") String alamat,
            @Field("image") String image
    );

    @Multipart
    @POST("profile-api?acces=upload")
    Call<ResponseBody> imageProfil(
            @Part MultipartBody.Part gambar
    );

    /*** GET USER ***/

//    http://reminder.mitraredex.com/application/api/reminder-api
//    http://reminder.mitraredex.com/application/api/reminder-api

    @GET("reminder-api")
    Call<ArrayList<ModelUser>> getUser();

    /*** KATEGORI ***/

    @GET("kategori-api")
    Call<ArrayList<ModelKategori>> getKategori();

    @Multipart
    @POST("kategori-api?acces=add")
    Call<ResponseBody> addKategori(
            @Part("nama") RequestBody nama,
            @Part MultipartBody.Part gambar
    );

    @FormUrlEncoded
    @POST("kategori-api?acces=edit")
    Call<ResponseBody> editKategori(
            @Query("value") String value,
            @Field("nama") String nama,
            @Field("image") String image
    );

    @GET("kategori-api?acces=delete")
    Call<ResponseBody> deleteKategori(
            @Query("value") String value
    );

    @Multipart
    @POST("kategori-api?acces=upload")
    Call<ResponseBody> imageKategori(
            @Part MultipartBody.Part gambar
    );

    /*** MATERI ***/

    @GET("materi-api")
    Call<ArrayList<ModelMateri>> getMateri(
            @Query("katId") String katId
    );

    @Multipart
    @POST("materi-api?acces=add")
    Call<ResponseBody> addMateri(
            @Part("judul") RequestBody judul,
            @Part("subject") RequestBody subject,
            @Part("katId") RequestBody katId,
            @Part MultipartBody.Part gambar
    );

    @FormUrlEncoded
    @POST("materi-api?acces=edit")
    Call<ResponseBody> editMateri(
            @Query("value") String value,
            @Field("judul") String judul,
            @Field("image") String image,
            @Field("subject") String subject
    );

    @GET("materi-api?acces=delete")
    Call<ResponseBody> deleteMateri(
            @Query("value") String value
    );

    @Multipart
    @POST("materi-api?acces=upload")
    Call<ResponseBody> imageMateri(
            @Part MultipartBody.Part gambar
    );

    /*** PENGENDALIAN ***/

    @GET("kendali-api")
    Call<ArrayList<ModelPengendalian>> getPengendalian();

    @Multipart
    @POST("kendali-api?acces=add")
    Call<ResponseBody> addKendali(
            @Part("judul") RequestBody judul,
            @Part("subject") RequestBody subject,
            @Part MultipartBody.Part gambar
    );

    @FormUrlEncoded
    @POST("kendali-api?acces=edit")
    Call<ResponseBody> editKendali(
            @Query("value") String value,
            @Field("judul") String judul,
            @Field("subject") String subject,
            @Field("image") String image
    );

    @GET("kendali-api?acces=delete")
    Call<ResponseBody> deleteKendali(
            @Query("value") String value
    );

    @Multipart
    @POST("kendali-api?acces=upload")
    Call<ResponseBody> imageKendali(
            @Part MultipartBody.Part gambar
    );

    /********** API USER **********/

    /*** MINUM OBAT ***/

    @GET("minumobat-api")
    Call<ArrayList<ModelObat>> getMinumObat(
            @Query("userId") String user_id);

    @FormUrlEncoded
    @POST("minumobat-api?acces=add")
    Call<ResponseBody> addObat(
            @Field("userId") String userId,
            @Field("nama") String nama,
            @Field("dosis") String dosis,
            @Field("frekuensi") String frekuensi,
            @Field("waktu") String waktu,
            @Field("sedia") String sedia,
            @Field("tanggalAwal") String tanggalAwal,
            @Field("tanggalAkhir") String tanggalAkhir
    );

    @FormUrlEncoded
    @POST("minumobat-api?acces=edit")
    Call<ResponseBody> editObat(
            @Query("value") String value,
            @Field("nama") String nama,
            @Field("dosis") String dosis,
            @Field("frekuensi") String frekuensi,
            @Field("waktu") String waktu,
            @Field("sedia") String sedia,
            @Field("tanggalAwal") String tanggalAwal,
            @Field("tanggalAkhir") String tanggalAkhir
    );

    @GET("minumobat-api?acces=delete")
    Call<ResponseBody> deleteObat(
            @Query("value") String value
    );

    /*** AKTIFITAS ***/

    @GET("aktifitas-api")
    Call<ArrayList<ModelAktifitas>> getAktifitas(
            @Query("userId") String user_id);

    @FormUrlEncoded
    @POST("aktifitas-api?acces=add")
    Call<ResponseBody> addAktifitas(
            @Field("userId") String userId,
            @Field("waktu") String waktu,
            @Field("jenis") String jenis,
            @Field("tanggalAwal") String tanggalAwal,
            @Field("tanggalAkhir") String tanggalAkhir
    );

    @FormUrlEncoded
    @POST("aktifitas-api?acces=edit")
    Call<ResponseBody> editAktifitas(
            @Query("value") String value,
            @Field("waktu") String waktu,
            @Field("jenis") String jenis,
            @Field("tanggalAwal") String tanggalAwal,
            @Field("tanggalAkhir") String tanggalAkhir
    );

    @GET("aktifitas-api?acces=delete")
    Call<ResponseBody> deleteAktifitas(
            @Query("value") String value
    );

    /*** DIET ***/

    @GET("diet-hipertensi")
    Call<ArrayList<ModelDiet>> getDiet(
            @Query("userId") String user_id);

    @FormUrlEncoded
    @POST("diet-hipertensi?acces=add")
    Call<ResponseBody> addDiet(
            @Field("userId") String userId,
            @Field("waktu") String waktu,
            @Field("cemilan") String cemilan,
            @Field("tanggalAwal") String tanggalAwal,
            @Field("tanggalAkhir") String tanggalAkhir
    );

    @FormUrlEncoded
    @POST("diet-hipertensi?acces=edit")
    Call<ResponseBody> editDiet(
            @Query("value") String value,
            @Field("waktu") String waktu,
            @Field("cemilan") String cemilan,
            @Field("tanggalAwal") String tanggalAwal,
            @Field("tanggalAkhir") String tanggalAkhir
    );

    @GET("diet-hipertensi")
    Call<ResponseBody> deleteDiet(
            @Query("acces") String acces,
            @Query("value") String value
    );

    /*** KESEHATAN ***/

    @GET("kesehatan-api")
    Call<ArrayList<ModelCekKesehatan>> getCekKesehatan(
            @Query("userId") String user_id);

    @FormUrlEncoded
    @POST("kesehatan-api?acces=add")
    Call<ResponseBody> addKesehatan(
            @Field("userId") String userId,
            @Field("waktu") String waktu,
            @Field("tempat") String tempat,
            @Field("tanggal") String tanggal
    );

    @FormUrlEncoded
    @POST("kesehatan-api?acces=edit")
    Call<ResponseBody> editKesehatan(
            @Query("value") String value,
            @Field("waktu") String waktu,
            @Field("tempat") String tempat,
            @Field("tanggal") String tanggal
    );

    @GET("kesehatan-api")
    Call<ResponseBody> deleteKesehatan(
            @Query("acces") String acces,
            @Query("value") String value
    );

}
