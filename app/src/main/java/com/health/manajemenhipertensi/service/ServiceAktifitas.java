package com.health.manajemenhipertensi.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.activity.AktifitasActivity;
import com.health.manajemenhipertensi.activity.MinumObatActivity;

public class ServiceAktifitas extends Service {
    String judul, ket,id;

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service Aktif", Toast.LENGTH_LONG).show();//        super.onCreate();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this, "MyAlarmService.onBind()", Toast.LENGTH_LONG).show();
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service.Destroy()", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Bundle bundle = intent.getExtras();
        judul = bundle.getString("judul");
        ket = bundle.getString("ket");
        id = bundle.getString("id");

        notifDua(judul,ket,id);
    }

    private void notifDua(String judul,String ket,String id){
        Intent intent = new Intent(this,AktifitasActivity.class);
        intent.putExtra("data","1");
        intent.putExtra("judul", ""+judul);
        intent.putExtra("ket",""+ket);
        intent.putExtra("id",""+id);
        //menginisialiasasi intent
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_aktivitas) //ikon notification
                .setContentTitle(""+judul) //judul konten
                .setContentIntent(pendingIntent)
                .setBadgeIconType(R.drawable.ic_aktivitas)
//                .setSound(alarmSound)
                .setAutoCancel(true)//untuk menswipe atau menghapus notification
                .setContentText(""+ket); //isi text

/*
Kemudian kita harus menambahkan Notification dengan menggunakan NotificationManager
 */

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, builder.build()
        );
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
