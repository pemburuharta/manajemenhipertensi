package com.health.manajemenhipertensi.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.activity.CekKesehatanActivity;
import com.health.manajemenhipertensi.activity.DietActivity;

public class ServiceDiet extends Service {

    String judul, ket,tgla,tglb,waktu,cemilan,id;

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service Aktif", Toast.LENGTH_LONG).show();//        super.onCreate();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this, "MyAlarmService.onBind()", Toast.LENGTH_LONG).show();
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service.Destroy()", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        Bundle bundle = intent.getExtras();

        judul = bundle.getString("judul");
        ket = bundle.getString("ket");
        tgla = bundle.getString("tgla");
        tglb = bundle.getString("tglb");
        waktu = bundle.getString("waktu");
        cemilan = bundle.getString("cemilan");
        id = bundle.getString("id");

        notifDua(judul,ket,tgla,tglb,waktu,cemilan,id);
    }

    private void notifDua(String judul,String ket,String tgla,String tglb,String waktu,String cemilan,String id){
//        Toast.makeText(this, "tes "+tempat+id, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this,DietActivity.class);
        intent.putExtra("data","1");
        intent.putExtra("judul", "Cek Kesehatan");
        intent.putExtra("ket","Saatnya Cek Kesehatan");
        intent.putExtra("tgla",""+tgla);
        intent.putExtra("tglb",""+tglb);
        intent.putExtra("waktu",""+waktu);
        intent.putExtra("cemilan",""+cemilan);
        intent.putExtra("id",""+id);
        //menginisialiasasi intent
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_diet) //ikon notification
                .setContentTitle(""+judul) //judul konten
                .setContentIntent(pendingIntent)
                .setBadgeIconType(R.drawable.ic_diet)
//                .setSound(alarmSound)
                .setAutoCancel(true)//untuk menswipe atau menghapus notification
                .setContentText(""+ket); //isi text

/*
Kemudian kita harus menambahkan Notification dengan menggunakan NotificationManager
 */

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, builder.build()
        );
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
