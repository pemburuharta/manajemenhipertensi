package com.health.manajemenhipertensi.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.health.manajemenhipertensi.R;
import com.health.manajemenhipertensi.activity.CekKesehatanActivity;
import com.health.manajemenhipertensi.activity.MinumObatActivity;

public class ServiceKesehatan extends Service {

    String judul, ket,tgl,waktu,tempat,id;
    android.support.v7.app.AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service Aktif", Toast.LENGTH_LONG).show();
//        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service.Destroy()", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        Bundle bundle = intent.getExtras();

        judul = bundle.getString("judul");
        ket = bundle.getString("ket");
        tgl = bundle.getString("tgl");
        waktu = bundle.getString("waktu");
        tempat = bundle.getString("tempat");
        id = bundle.getString("id");

        notifDua(judul,ket,tgl,waktu,tempat,id);
    }

    private void notifDua(String judul,String ket,String tgl,String waktu,String tempat,String id){
//        Toast.makeText(this, "tes "+tempat+id, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this,CekKesehatanActivity.class);
        intent.putExtra("data","1");
        intent.putExtra("judul", "Cek Kesehatan");
        intent.putExtra("ket","Saatnya Cek Kesehatan");
        intent.putExtra("tgl",""+tgl);
        intent.putExtra("waktu",""+waktu);
        intent.putExtra("tempat",""+tempat);
        intent.putExtra("id",""+id);
        //menginisialiasasi intent
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_cekkesehatan) //ikon notification
                .setContentTitle(""+judul) //judul konten
                .setContentIntent(pendingIntent)
                .setBadgeIconType(R.drawable.ic_cekkesehatan)
//                .setSound(alarmSound)
                .setAutoCancel(true)//untuk menswipe atau menghapus notification
                .setContentText(""+ket); //isi text

/*
Kemudian kita harus menambahkan Notification dengan menggunakan NotificationManager
 */

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, builder.build()
        );
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

}
