package com.health.manajemenhipertensi.Helper;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Helper {

    public Helper(){

    }

    public String convertRupiah(Integer x) {

        Locale localeID = new Locale("in", "ID");
        NumberFormat format = NumberFormat.getCurrencyInstance(localeID);
        String hasilConvert = format.format((double) x);

        return hasilConvert;

    }

    public String convertDateFormat(String date, String frmtlama) {

        String hasil= "";

        final String formatBaru = "dd MMMM yyyy";

        SimpleDateFormat dateFormat = new SimpleDateFormat(frmtlama);
        try {
            Date dd = dateFormat.parse(date);
            dateFormat.applyPattern(formatBaru);
            hasil = dateFormat.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;

    }

    public String convertDateFormat2(String date, String frmtlama) {

        String hasil= "";

        final String formatBaru = "dd MMMM";

        SimpleDateFormat dateFormat = new SimpleDateFormat(frmtlama);
        try {
            Date dd = dateFormat.parse(date);
            dateFormat.applyPattern(formatBaru);
            hasil = dateFormat.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;

    }

    public String convertDateFormatNew(String date, String frmtlama) {

        String hasil= "";

        final String formatBaru = "dd MMMM yyyy HH:mm";

        SimpleDateFormat dateFormat = new SimpleDateFormat(frmtlama);
        try {
            Date dd = dateFormat.parse(date);
            dateFormat.applyPattern(formatBaru);
            hasil = dateFormat.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;

    }

    public String convertJam(String date, String frmtlama) {

        String hasil= "";

        final String formatBaru = "k";

        SimpleDateFormat dateFormat = new SimpleDateFormat(frmtlama);
        try {
            Date dd = dateFormat.parse(date);
            dateFormat.applyPattern(formatBaru);
            hasil = dateFormat.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;

    }
    public String convertMenit(String date, String frmtlama) {

        String hasil= "";

        final String formatBaru = "m";

        SimpleDateFormat dateFormat = new SimpleDateFormat(frmtlama);
        try {
            Date dd = dateFormat.parse(date);
            dateFormat.applyPattern(formatBaru);
            hasil = dateFormat.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;

    }
    public String convertDetik(String date, String frmtlama) {

        String hasil= "";

        final String formatBaru = "s";

        SimpleDateFormat dateFormat = new SimpleDateFormat(frmtlama);
        try {
            Date dd = dateFormat.parse(date);
            dateFormat.applyPattern(formatBaru);
            hasil = dateFormat.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;

    }
}
