package com.health.manajemenhipertensi;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.health.manajemenhipertensi.Helper.Helper;
import com.health.manajemenhipertensi.Helper.SharedPref;
import com.health.manajemenhipertensi.activity.AddAktivitasActivity;
import com.health.manajemenhipertensi.activity.AddCekKesehatanActivity;
import com.health.manajemenhipertensi.activity.AddObatActivity;
import com.health.manajemenhipertensi.activity.AddPengingatDietActivity;
import com.health.manajemenhipertensi.activity.AktifitasActivity;
import com.health.manajemenhipertensi.activity.CekKesehatanActivity;
import com.health.manajemenhipertensi.activity.DietActivity;
import com.health.manajemenhipertensi.activity.KategoriActivity;
import com.health.manajemenhipertensi.activity.MinumObatActivity;
import com.health.manajemenhipertensi.activity.PengendalianActivity;
import com.health.manajemenhipertensi.activity.ProfilActivity;
import com.health.manajemenhipertensi.adapter.AdapterAktifitas;
import com.health.manajemenhipertensi.adapter.AdapterCekKesehatan;
import com.health.manajemenhipertensi.adapter.AdapterDiet;
import com.health.manajemenhipertensi.adapter.AdapterObat;
import com.health.manajemenhipertensi.model.ModelAktifitas;
import com.health.manajemenhipertensi.model.ModelCekKesehatan;
import com.health.manajemenhipertensi.model.ModelDiet;
import com.health.manajemenhipertensi.model.ModelObat;
import com.health.manajemenhipertensi.model.ModelUser;
import com.health.manajemenhipertensi.retrofit.APIService;
import com.health.manajemenhipertensi.retrofit.ApiConfig;
import com.health.manajemenhipertensi.service.ServiceAktifitas;
import com.health.manajemenhipertensi.service.ServiceDiet;
import com.health.manajemenhipertensi.service.ServiceKesehatan;
import com.health.manajemenhipertensi.service.ServiceObat;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private CardView homeCvDefinisi;
    private CardView homeCvPengendalian;
    private CardView homeCvMinumobat;
    private CardView homeCvAktifitas;
    private CardView homeCvDiet;
    private CardView homeCvCekkesehatan;
    private RelativeLayout homeCvIdentitas;
    private ImageView homeIvSetting;
    private RelativeLayout homeDivAvatar;
    private CircleImageView homeIvAvatar;
    private TextView homeTvUsername;

    SharedPref pref;
    Calendar calendar;
    ArrayList<ModelCekKesehatan> dataList = new ArrayList<>();
    ArrayList<ModelObat> dataObat = new ArrayList<>();
    ArrayList<ModelAktifitas> dataAktifitas = new ArrayList<>();
    ArrayList<ModelDiet> dataDiet = new ArrayList<>();
    String tanggal,waktu;
    ModelUser data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        calendar = Calendar.getInstance();
        int timeOfDay = calendar.get(Calendar.HOUR_OF_DAY);

        if (pref.getAvatar() != null) {

            Picasso.with(this)
                    .load("http://reminder.mitraredex.com/assets/user/" + pref.getAvatar())
                    .into(homeIvAvatar);

        } else {
            homeIvAvatar.setImageResource(R.drawable.ic_profil);
        }

        if (pref.getRule().equals("USER")){

            if(timeOfDay >= 0 && timeOfDay < 10){
                homeTvUsername.setText("Selamat Pagi " + pref.getName() + " :)");
            }else if(timeOfDay >= 10 && timeOfDay < 16){
                homeTvUsername.setText("Selamat Siang " + pref.getName() + " :)");
            }else if(timeOfDay >= 16 && timeOfDay < 18){
                homeTvUsername.setText("Selamat Sore " + pref.getName() + " :)");
            }else if(timeOfDay >= 18 && timeOfDay < 24){
                homeTvUsername.setText("Selamat Malam " + pref.getName() + " :)");
            }

        } else {

            data = getIntent().getParcelableExtra("data");

            if (!data.getUIMAGE().isEmpty()) {

                Picasso.with(this)
                        .load("http://reminder.mitraredex.com/assets/user/" + data.getUIMAGE())
                        .into(homeIvAvatar);

            } else {
                homeIvAvatar.setImageResource(R.drawable.ic_profil);
            }

            int timeOfDay2 = calendar.get(Calendar.HOUR_OF_DAY);

            if(timeOfDay2 >= 0 && timeOfDay < 10){
                homeTvUsername.setText("Selamat Pagi " + data.getUNAME() + " :)");
            }else if(timeOfDay2 >= 10 && timeOfDay < 16){
                homeTvUsername.setText("Selamat Siang " + data.getUNAME() + " :)");
            }else if(timeOfDay2 >= 16 && timeOfDay < 18){
                homeTvUsername.setText("Selamat Sore " + data.getUNAME() + " :)");
            }else if(timeOfDay2 >= 18 && timeOfDay < 24){
                homeTvUsername.setText("Selamat Malam " + data.getUNAME() + " :)");
            }

            homeIvSetting.setVisibility(View.GONE);

        }


        homeIvSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfilActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        homeCvDefinisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), KategoriActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        homeCvPengendalian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PengendalianActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        homeCvCekkesehatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),CekKesehatanActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","0");
                startActivity(intent);
            }
        });

        homeCvAktifitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),AktifitasActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","0");
                startActivity(intent);
            }
        });

        homeCvMinumobat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),MinumObatActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","0");
                startActivity(intent);
            }
        });

        homeCvDiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),DietActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("data","0");
                startActivity(intent);
            }
        });

        getCurrentDate();
    }

    private void getCekKesehatan() {
        dataList.removeAll(dataList);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getCekKesehatan(
                pref.getIdUser()).enqueue(new Callback<ArrayList<ModelCekKesehatan>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelCekKesehatan>> call, Response<ArrayList<ModelCekKesehatan>> response) {
                if (response.isSuccessful()) {

                    dataList = response.body();
                    if (dataList.get(0).getTKBIGID().isEmpty()) {

                    } else {

                        kirimServiceKesehatan(dataList);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelCekKesehatan>> call, Throwable t) {
//                Toast.makeText(MainActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Periksa Koneksi Anda...")
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getCekKesehatan();
                            }
                        })
                        .show();
            }
        });

    }

    private void getMinumObat() {
        dataObat.removeAll(dataObat);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getMinumObat(pref.getIdUser()).enqueue(new Callback<ArrayList<ModelObat>>() {
            @Override
            public void onResponse(Call<ArrayList<ModelObat>> call, Response<ArrayList<ModelObat>> response) {
                if (response.isSuccessful()) {

                    dataObat = response.body();

                    if (dataObat.get(0).getTMOBIGID().isEmpty()) {

                    } else {
                        kirimServiceObat(dataObat);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModelObat>> call, Throwable t) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Periksa Koneksi Anda...")
                        .setCancelable(false)
                        .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getMinumObat();
                            }
                        })
                        .show();
                //                Toast.makeText(MainActivity.this, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getAktifitas() {
        dataAktifitas.removeAll(dataAktifitas);
        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getAktifitas(pref.getIdUser())
                .enqueue(new Callback<ArrayList<ModelAktifitas>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ModelAktifitas>> call, Response<ArrayList<ModelAktifitas>> response) {
                        if (response.isSuccessful()) {

                            dataAktifitas = response.body();
                            if (dataAktifitas.get(0).getTABIGID().isEmpty()) {

                            } else {
                                kirimServiceAktifitas(dataAktifitas);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ModelAktifitas>> call, Throwable t) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Periksa Koneksi Anda...")
                                .setCancelable(false)
                                .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        getAktifitas();
                                    }
                                })
                                .show();
                    }
                });

    }

    private void getDiet() {
        dataDiet.removeAll(dataDiet);

        APIService apiService = ApiConfig.getInstanceRetrofit();
        apiService.getDiet(pref.getIdUser())
                .enqueue(new Callback<ArrayList<ModelDiet>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ModelDiet>> call, Response<ArrayList<ModelDiet>> response) {
                        if (response.isSuccessful()) {

                            dataDiet = response.body();
                            if (dataDiet.get(0).getTHBIGID().isEmpty()) {

                            } else {
                              kirimServiceDiet(dataDiet);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ArrayList<ModelDiet>> call, Throwable t) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setMessage("Periksa Koneksi Anda...")
                                .setCancelable(false)
                                .setPositiveButton("Ulangi", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        getDiet();
                                    }
                                })
                                .show();
                    }
                });

    }

    private void kirimServiceKesehatan(ArrayList<ModelCekKesehatan> dataList) {
//        Toast.makeText(this, ""+dataList.size(), Toast.LENGTH_SHORT).show();

        for (int i = 0; i <dataList.size() ; i++) {
            if (dataList.get(i).getTKTGL().equals(tanggal)){
                String jam = new Helper().convertJam(dataList.get(i).getTKWAKTU(),"hh:mm:ss");
                String menit = new Helper().convertMenit(dataList.get(i).getTKWAKTU(),"hh:mm:ss");
                String detik = new Helper().convertDetik(dataList.get(i).getTKWAKTU(),"hh:mm:ss");
//                Toast.makeText(this, ""+dataList.get(i).getTKTEMPAT(), Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(MainActivity.this, ServiceKesehatan.class);
                myIntent.putExtra("judul", "Cek Kesehatan");
                myIntent.putExtra("ket","Saatnya Cek Kesehatan");

                myIntent.putExtra("tgl",""+dataList.get(i).getTKTGL());
                myIntent.putExtra("waktu",""+dataList.get(i).getTKWAKTU());
                myIntent.putExtra("tempat",""+dataList.get(i).getTKTEMPAT());
                myIntent.putExtra("id",""+dataList.get(i).getTKBIGID());

                Calendar calNow = Calendar.getInstance();
                Calendar calendar = (Calendar) calNow.clone();

//                        Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(jam));
                calendar.set(Calendar.MINUTE, Integer.parseInt(menit));
                calendar.set(Calendar.SECOND, 4);

                if (calendar.compareTo(calNow) <= 0) {
                    //jika ternyata waktu lewat maka alarm akan di atur untuk besok
                    calendar.add(Calendar.DATE, 1);
                }

                PendingIntent pendingIntent = PendingIntent.getService(MainActivity.this, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //LOLLIPOP 21 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pendingIntent);
                    alarmManager.setAlarmClock(alarmClockInfo, pendingIntent);
                }
                //KITKAT 19 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //FOR BELOW KITKAT ALL DEVICES
                else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
            }else {
                Toast.makeText(this, "tak sesuai", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void kirimServiceObat(ArrayList<ModelObat>list) {

        if (list.size() == 0){

        }else {
            for (int i = 0; i <list.size() ; i++) {
                String jam = new Helper().convertJam(list.get(i).getTMOWAKTU(),"hh:mm:ss");
                String menit = new Helper().convertMenit(list.get(i).getTMOWAKTU(),"hh:mm:ss");
                String detik = new Helper().convertDetik(list.get(i).getTMOWAKTU(),"hh:mm:ss");

//                Toast.makeText(this, ""+dataList.get(i).getTKTEMPAT(), Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(MainActivity.this, ServiceObat.class);
                myIntent.putExtra("judul", "Obat");
                myIntent.putExtra("ket",""+"Saatnya Minum Obat");

                myIntent.putExtra("id",""+list.get(i).getTMOBIGID());
                myIntent.putExtra("nama",""+list.get(i).getTMONAMA());
                myIntent.putExtra("dosis",""+list.get(i).getTMODOSIS());
                myIntent.putExtra("frekuensi",""+list.get(i).getTMOFREKUENSI());
                myIntent.putExtra("waktu",""+list.get(i).getTMOWAKTU());
                myIntent.putExtra("tersedia",""+list.get(i).getTMOKETERSEDIAAN());
                myIntent.putExtra("tgla",""+list.get(i).getTMOSTARTTGL());
                myIntent.putExtra("tglb",""+list.get(i).getTMOENDTGL());

                Calendar calNow = Calendar.getInstance();
                Calendar calendar = (Calendar) calNow.clone();

//                        Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(jam));
                calendar.set(Calendar.MINUTE, Integer.parseInt(menit));
                calendar.set(Calendar.SECOND, 4);

                if (calendar.compareTo(calNow) <= 0) {
                    //jika ternyata waktu lewat maka alarm akan di atur untuk besok
                    calendar.add(Calendar.DATE, 1);
                }

                PendingIntent pendingIntent = PendingIntent.getService(MainActivity.this, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //LOLLIPOP 21 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pendingIntent);
                    alarmManager.setAlarmClock(alarmClockInfo, pendingIntent);
                }
                //KITKAT 19 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //FOR BELOW KITKAT ALL DEVICES
                else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
            }

        }

    }

    private void kirimServiceAktifitas(ArrayList<ModelAktifitas>list) {

        if (list.size() == 0){

        }else {
            for (int i = 0; i <list.size() ; i++) {
                String jam = new Helper().convertJam(list.get(i).getTAWAKTU(),"hh:mm:ss");
                String menit = new Helper().convertMenit(list.get(i).getTAWAKTU(),"hh:mm:ss");
                String detik = new Helper().convertDetik(list.get(i).getTAWAKTU(),"hh:mm:ss");

//                Toast.makeText(this, ""+dataList.get(i).getTKTEMPAT(), Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(MainActivity.this, ServiceAktifitas.class);
                myIntent.putExtra("judul", "Aktifitas");
                myIntent.putExtra("ket",""+"Saatnya Beraktifitas");

                myIntent.putExtra("id",""+list.get(i).getTABIGID());

                Calendar calNow = Calendar.getInstance();
                Calendar calendar = (Calendar) calNow.clone();

//                        Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(jam));
                calendar.set(Calendar.MINUTE, Integer.parseInt(menit));
                calendar.set(Calendar.SECOND, 4);

                if (calendar.compareTo(calNow) <= 0) {
                    //jika ternyata waktu lewat maka alarm akan di atur untuk besok
                    calendar.add(Calendar.DATE, 1);
                }

                PendingIntent pendingIntent = PendingIntent.getService(MainActivity.this, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //LOLLIPOP 21 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pendingIntent);
                    alarmManager.setAlarmClock(alarmClockInfo, pendingIntent);
                }
                //KITKAT 19 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //FOR BELOW KITKAT ALL DEVICES
                else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
            }

        }

    }

    private void kirimServiceDiet(ArrayList<ModelDiet> dataList) {
//        Toast.makeText(this, ""+dataList.size(), Toast.LENGTH_SHORT).show();
        if (dataList.size() == 0) {

        } else {
            for (int i = 0; i < dataList.size(); i++) {
                String jam = new Helper().convertJam(dataList.get(i).getTHWAKTU(),"hh:mm:ss");
                String menit = new Helper().convertMenit(dataList.get(i).getTHWAKTU(),"hh:mm:ss");
                String detik = new Helper().convertDetik(dataList.get(i).getTHWAKTU(),"hh:mm:ss");
//                Toast.makeText(this, ""+dataList.get(i).getTKTEMPAT(), Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(MainActivity.this, ServiceDiet.class);
                myIntent.putExtra("judul", "Cek Diet");
                myIntent.putExtra("ket","Saatnya Diet");

                myIntent.putExtra("tgla",""+dataList.get(i).getTHSTARTTGL());
                myIntent.putExtra("tglb",""+dataList.get(i).getTHENDTGL());
                myIntent.putExtra("waktu",""+dataList.get(i).getTHWAKTU());
                myIntent.putExtra("cemilan",""+dataList.get(i).getTHCEMILAN());
                myIntent.putExtra("id",""+dataList.get(i).getTHBIGID());

                Calendar calNow = Calendar.getInstance();
                Calendar calendar = (Calendar) calNow.clone();

//                        Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(jam));
                calendar.set(Calendar.MINUTE, Integer.parseInt(menit));
                calendar.set(Calendar.SECOND, 4);

                if (calendar.compareTo(calNow) <= 0) {
                    //jika ternyata waktu lewat maka alarm akan di atur untuk besok
                    calendar.add(Calendar.DATE, 1);
                }

                PendingIntent pendingIntent = PendingIntent.getService(MainActivity.this, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //LOLLIPOP 21 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pendingIntent);
                    alarmManager.setAlarmClock(alarmClockInfo, pendingIntent);
                }
                //KITKAT 19 OR ABOVE
                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
                //FOR BELOW KITKAT ALL DEVICES
                else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP,
                            calendar.getTimeInMillis(), pendingIntent);
                }
            }

        }
    }

    public void getCurrentDate() {
        final Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

        tanggal = sdf2.format(c.getTime());
        waktu = sdf.format(c.getTime());
//        Toast.makeText(this, ""+waktu, Toast.LENGTH_SHORT).show();
        getCekKesehatan();
        getMinumObat();
        getAktifitas();
        getDiet();

    }

    private void initView() {
        homeCvDefinisi = findViewById(R.id.home_cv_definisi);
        homeCvPengendalian = findViewById(R.id.home_cv_pengendalian);
        homeCvMinumobat = findViewById(R.id.home_cv_minumobat);
        homeCvAktifitas = findViewById(R.id.home_cv_aktifitas);
        homeCvDiet = findViewById(R.id.home_cv_diet);
        homeCvCekkesehatan = findViewById(R.id.home_cv_cekkesehatan);
        homeCvIdentitas = findViewById(R.id.home_cv_identitas);
        homeIvSetting = findViewById(R.id.home_iv_setting);
        homeDivAvatar = findViewById(R.id.home_div_avatar);
        homeIvAvatar = findViewById(R.id.home_iv_avatar);
        homeTvUsername = findViewById(R.id.home_tv_username);
    }

    @Override
    public void onBackPressed() {

        if (pref.getRule().equals("USER")){

            final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setCancelable(true);
            dialog.setMessage("Apakah anda yakin ingin keluar ?");
            dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                    startMain.addCategory(Intent.CATEGORY_HOME);
                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startMain);
                }
            });

            dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();

        } else {
            super.onBackPressed();
        }

    }

}
